#!/bin/bash

################################
# Installation script
# -------------------
#
# The only argument is the directory where the python virtualenv will be created
# The script must be run from its directory
################################

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. 1 needed."
fi
if [ $# -gt 1 ]
  then
    echo "Too many arguments supplied. Only 1 needed"
fi

mkdir -p $1
cd $1
test -d venv || python3 -m virtualenv venv
$1/venv/bin/pip install -Ur requirements.txt
source $1/venv/bin/activate
export PYTHONPATH=./Grappe
python manage.py install
python manage.py makemigrations
python manage.py migrate

read -p 'Do you want to fill the db with fake data? [y/n] ' filldb
if [[ $filldb == 'y' ]]
  then
	python manage.py createsuperuser admin admin
	python manage.py filldb
fi

echo 'Installation done.'
