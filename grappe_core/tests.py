# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
from django.test import TestCase
from django.contrib.auth.models import User

from grappe_core.compute import compute_accounts
from grappe_core.models import Share, Grappe


def create_users():
    users = list()
    for name in ['bob', 'peter', 'julie', 'marie', 'jacques', 'pierre', 'olivier', 'jeanne', 'élodie', 'cécile']:
        user = User.objects.create_user(name, password='aazz00..')
        user.is_superuser = False
        user.is_staff = False
        user.save()
        users.append(user)
    return users


def create_grappe(users):
    g = Grappe(name='Les Volontaires')
    g.save()
    g.members.set(users)
    g.save()
    return g


class SharesTestCase(TestCase):
    def setUp(self):
        users = create_users()
        grappe = create_grappe(users)

        Share.objects.create_share(User.objects.get(username='bob'),
                                   [User.objects.get(username='peter'), User.objects.get(username='marie')],
                                   1,
                                   10,
                                   'travaux',
                                   datetime.datetime.now(),
                                   'Réparation du mur',
                                   grappe)

        Share.objects.create_share(User.objects.get(username='jacques'),
                                   [User.objects.get(username='marie'), User.objects.get(username='pierre')],
                                   2,
                                   50,
                                   'cuisine',
                                   datetime.datetime.now(),
                                   'Préparation de pots de ratatouille',
                                   grappe)

        Share.objects.create_share(User.objects.get(username='élodie'),
                                   [User.objects.get(username='cécile')],
                                   3,
                                   100,
                                   'Mécanique',
                                   datetime.datetime.now(),
                                   'Réparation de la voiture',
                                   grappe)

    def test_accounts(self):
        """Validate the accounts"""
        grappe = Grappe.objects.get(name='Les Volontaires')
        accounts = compute_accounts(grappe, *grappe.members.all())

        expected_results = [
            {'user': User.objects.get(username='bob'),
             'time': 1,
             'money': 10},
            {'user': User.objects.get(username='peter'),
             'time': -0.5,
             'money': -5},
            {'user': User.objects.get(username='julie'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='marie'),
             'time': -1 - 0.5,
             'money': -25 - 5},
            {'user': User.objects.get(username='jacques'),
             'time': 2,
             'money': 50},
            {'user': User.objects.get(username='pierre'),
             'time': -1,
             'money': -25},
            {'user': User.objects.get(username='olivier'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='jeanne'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='élodie'),
             'time': 3,
             'money': 100},
            {'user': User.objects.get(username='cécile'),
             'time': -3,
             'money': -100}
        ]

        self.assertEqual(accounts, expected_results)
