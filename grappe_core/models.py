# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import uuid
import random
from django.contrib.auth.models import User
from django.db import models, transaction
from django.utils.text import slugify
from grappe_core.utils import get_file_upload_path


def _gen_slug_suffix():
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    suffix = ''.join((random.choice(chars)) for _ in range(6))
    return suffix


class Grappe(models.Model):
    slug = models.SlugField(primary_key=True, unique=True)
    name = models.CharField(max_length=30)
    members = models.ManyToManyField(User, related_name='grappe_members')
    logo = models.ImageField(blank=True, upload_to=get_file_upload_path)
    time_unit = models.CharField(max_length=20, default='demie-journée')
    money_unit = models.CharField(max_length=20, default='€')
    min_time_bound = models.FloatField(default=0)
    max_time_bound = models.FloatField(default=0)
    min_money_bound = models.FloatField(default=0)
    max_money_bound = models.FloatField(default=0)

    def save(self, *args, **kwargs):
        if self._state.adding:
            while (not self.slug) or Grappe.objects.filter(slug=self.slug).exists():
                self.slug = f"{slugify(self.name)}-{_gen_slug_suffix()}"
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class ShareManager(models.Manager):

    def create_share(self, from_member, to_members, time, money, tag, date, description, grappe, from_issue=False):
        # Create a new share
        share = Share(from_member=from_member,
                      time=time,
                      money=money,
                      tag=tag,
                      date=date,
                      description=description,
                      grappe=grappe,
                      from_issue=from_issue)
        share.save()

        # Add the 'to' members
        for m in to_members:
            share.to_members.add(m)

        return share


class Share(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    from_member = models.ForeignKey(User, on_delete=models.CASCADE)
    to_members = models.ManyToManyField(User, related_name='to_members', blank=True) # with one or several member(s)
    time = models.FloatField()
    money = models.FloatField()
    tag = models.CharField(blank=True, max_length=30)
    date = models.DateField()
    description = models.CharField(max_length=150)
    grappe = models.ForeignKey(Grappe, on_delete=models.CASCADE)

    objects = ShareManager()

    #  Trick because from_issue does not need to be saved in the database
    #  and can be used as an instance attribute
    def __init__(self, *args, **kwargs):
        try:
            self.from_issue = kwargs.pop('from_issue')
        except KeyError:
            self.from_issue = False
        super().__init__(*args, **kwargs)

    def delete(self, *args, **kwargs):
        GrappeNewsfeed.add_event(label="Échange supprimé",
                                 text=f"{self.description}",
                                 grappe=self.grappe)
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.description


class Idea(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=500)
    grappe = models.ForeignKey(Grappe, on_delete=models.CASCADE)

    def delete(self, *args, **kwargs):
        GrappeNewsfeed.add_event(label="Une envie a été supprimée",
                                 text=f"{self.member} {self.description}",
                                 grappe=self.grappe)
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.description


class Pool(models.Model):
    slug = models.SlugField(primary_key=True, unique=True)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=5000)
    members = models.ManyToManyField(User)
    grappe = models.ForeignKey(Grappe, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if self._state.adding:
            while (not self.slug) or Pool.objects.filter(slug=self.slug).exists():
                self.slug = f"{slugify(self.name)}-{_gen_slug_suffix()}"
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        GrappeNewsfeed.add_event(label="Un commun a été supprimé",
                                 text=f"{self.name} : {self.description}",
                                 grappe=self.grappe)
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name


class Issue(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    description = models.CharField(max_length=500)
    date = models.DateField()
    shares = models.ManyToManyField(Share, related_name='shares')
    pool = models.ForeignKey(Pool, on_delete=models.CASCADE)

    @transaction.atomic
    def save(self, *args, **kwargs):
        if hasattr(self, 'absorb'):
            shares = list()
            for share in self.absorb():
                share = Share.objects.create_share(share['from'],
                                                   share['to'],
                                                   share['time'],
                                                   share['money'],
                                                   'Resolution',
                                                   share['date'],
                                                   share['description'],
                                                   self.pool.grappe,
                                                   from_issue=True)
                shares.append(share)
        else:
            raise Exception('Child does not have absorb() function implemented')

        super().save(*args, **kwargs)

        # Clean first
        for s in self.shares.all():
            s.from_issue = True
            s.delete()
        # And set again the shares.
        self.shares.set(shares)

    def delete(self, *args, **kwargs):
        GrappeNewsfeed.add_event(label="Equilibrage supprimé",
                                 text=f"{self.description}",
                                 pool=self.pool)
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.description


class Resource(models.Model):

    slug = models.SlugField(primary_key=True, unique=True)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=2000)
    pool = models.ForeignKey(Pool, on_delete=models.CASCADE)
    tag = models.CharField(max_length=30, blank=True, default='')

    def save(self, *args, **kwargs):
        while (not self.slug) or Resource.objects.filter(slug=self.slug).exists():
            self.slug = f"{slugify(self.name)}-{_gen_slug_suffix()}"
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        GrappeNewsfeed.add_event(label="Une ressource a été supprimée",
                                 text=f"{self.name}",
                                 pool=self.pool)
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name


class GrappeNewsfeed(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    label = models.CharField(max_length=100)
    text = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True) # FIXME: prendre en compte le fuseau horaire (ou c'est que chez moi que ça marche pas ?)
    pool = models.ForeignKey(Pool, on_delete=models.SET_NULL, blank=True, null=True)
    grappe = models.ForeignKey(Grappe, on_delete=models.CASCADE)

    save_and_update_registered_classes = []

    @classmethod
    def add_event(cls, label, text, grappe=None, pool=None):
        if grappe is None and pool is None:
            raise ValueError("grappe or pool must be specified")
        if grappe is None:
            grappe = pool.grappe
        cls.objects.create(label=label, text=text, grappe=grappe, pool=pool)

    @classmethod
    def save_update_delete_events(cls, class_to_register):
        if not hasattr(class_to_register, 'pool') and not hasattr(class_to_register, 'grappe'):
            raise AttributeError("The class you are registering does not contain a 'grappe' or a 'pool' attribute")

        if class_to_register not in cls.save_and_update_registered_classes:
            cls.save_and_update_registered_classes.append(class_to_register)

        def new_delete(self_, *args, **kwargs):
            if hasattr(self_, "pool"):
                pool = self_.pool
                grappe = self_.pool.grappe
            else:
                pool = None
                grappe = self_.grappe
            GrappeNewsfeed.add_event(label="Supprimé(e)",
                                     text=f"{self_}",
                                     grappe=grappe,
                                     pool=pool)
            super(class_to_register, self_).delete()

        class_to_register.delete = new_delete

        return class_to_register


class UserProfile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    presentation = models.CharField(max_length=2000)
    avatar = models.ImageField(blank=True, upload_to=get_file_upload_path)
