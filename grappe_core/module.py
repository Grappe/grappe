# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from importlib import import_module
from django.urls.conf import path
import inspect


class PoolInfo(object):

    def __init__(self):
        self.create_form_class = None
        self.update_form_class = None
        self.view_class = None

        self.create_template_name = "form.html"
        self.update_template_name = "form.html"
        self.delete_template_name = "form.html"

        self.create_urlpattern = None
        self.update_urlpattern = None
        self.delete_urlpattern = None

    @property
    def model_class(self):
        return self.create_form_class.Meta.model


class PoolsRegistry(object):
    pools = dict()

    @classmethod
    def get_or_create_pool_info(cls):
        module_name = inspect.currentframe().f_back.f_back.f_globals['__package__']
        try:
            pool_info = cls.pools[module_name]
        except KeyError:
            pool_info = PoolInfo()
            cls.pools[module_name] = pool_info
        return pool_info, module_name

    # Form registration

    @classmethod
    def set_pool_form_class(cls, form_class):
        pool_info, _ = cls.get_or_create_pool_info()
        pool_info.create_form_class = form_class
        pool_info.update_form_class = form_class

    @classmethod
    def set_pool_create_form_class(cls, form_class):
        pool_info, _ = cls.get_or_create_pool_info()
        pool_info.create_form_class = form_class

    @classmethod
    def set_pool_update_form_class(cls, form_class):
        pool_info, _ = cls.get_or_create_pool_info()
        pool_info.update_form_class = form_class

    # Urlpattern registration

    @classmethod
    def set_pool_create_urlpattern(cls, urlpattern):
        pool_info, _ = cls.get_or_create_pool_info()
        urlpattern.name = 'create_pool'
        pool_info.create_urlpattern = urlpattern

    @classmethod
    def set_pool_update_urlpattern(cls, urlpattern):
        pool_info, _ = cls.get_or_create_pool_info()
        urlpattern.name = 'update_pool'
        pool_info.update_urlpattern = urlpattern

    @classmethod
    def set_pool_delete_urlpattern(cls, urlpattern):
        pool_info, _ = cls.get_or_create_pool_info()
        urlpattern.name = 'delete_pool'
        pool_info.delete_urlpattern = urlpattern

    #  Details view registration
    @classmethod
    def set_pool_details_view_class(cls, view_class):
        pool_info, _ = cls.get_or_create_pool_info()
        pool_info.view_class = view_class

    # Template name registration

    @classmethod
    def set_pool_create_templatename(cls, template_name):
        pool_info, _ = cls.get_or_create_pool_info()
        pool_info.create_template_name = template_name

    @classmethod
    def set_pool_update_templatename(cls, template_name):
        pool_info, _ = cls.get_or_create_pool_info()
        pool_info.update_template_name = template_name

    @classmethod
    def set_pool_delete_templatename(cls, template_name):
        pool_info, _ = cls.get_or_create_pool_info()
        pool_info.delete_template_name = template_name

    # Validation

    @classmethod
    def finalize_registration(cls):
        from grappe_ui.views import PoolCreateView, PoolUpdateView, PoolDeleteView

        pool_info, module_name = cls.get_or_create_pool_info()

        # Verification on mandatory pool_info attributes
        assert pool_info.create_form_class is not None
        assert pool_info.update_form_class is not None
        assert pool_info.view_class is not None
        assert pool_info.create_template_name is not None
        assert pool_info.update_template_name is not None
        assert pool_info.delete_template_name is not None
        assert pool_info.model_class is not None

        # Verification of mandatory model attributes
        pool_info.model_class.verbose_name

        # Set default urlpatterns
        pool_urls = import_module(f'{module_name}.urls')
        pool_urls.urlpatterns += [pool_info.create_urlpattern or path('create/new/',
                                                                      PoolCreateView.as_view(model=pool_info.model_class,
                                                                                             form_class=pool_info.create_form_class,
                                                                                             template_name=f'{module_name}/{pool_info.create_template_name}'),
                                                                      name='create_pool'),
                                  path('<slug>/', pool_info.view_class.as_view(), name='pool_details'),
                                  pool_info.update_urlpattern or path('<slug>/update/',
                                                                      PoolUpdateView.as_view(model=pool_info.model_class,
                                                                                             form_class=pool_info.update_form_class,
                                                                                             template_name=f'{module_name}/{pool_info.update_template_name}'),
                                                                      name='update_pool'),
                                  pool_info.delete_urlpattern or path('<slug>/delete/',
                                                                      PoolDeleteView.as_view(model=pool_info.model_class,
                                                                                             template_name=f'{module_name}/{pool_info.delete_template_name}'),
                                                                      name='delete_pool')]
