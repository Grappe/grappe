# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from colour import Color
from django.db.models import Q

from grappe_core.models import Share


def _select_shares(grappe, user):
    """
    Select the shares of this user in a given grappe.

    :param grappe: the grappe
    :param user: the user
    :return:
    """
    shares = Share.objects.filter(Q(grappe=grappe),
                                  Q(from_member__username=user.username)
                                  | Q(to_members__username__contains=user.username)
                                  )
    shares = shares.distinct()
    return shares


def build_history(grappe, user):
    """
    Build the history of a user in a grappe.

    :param grappe:
    :param user:
    :return: the dictionary of the history
    """

    def __process_history_item(share):
        history_item = dict()
        history_item['share'] = share

        nb_members = len(share.to_members.all())

        if user == share.from_member and user not in share.to_members.all():
            # The user is fully giving to others
            history_item['computed_time'] = share.time
            history_item['computed_money'] = share.money
        elif user == share.from_member and user in share.to_members.all():
            # The user if giving to himself and others
            history_item['computed_time'] = share.time - share.time / nb_members
            history_item['computed_money'] = share.money - share.money / nb_members
        else:
            # The user is receiving
            history_item['computed_time'] = -share.time / nb_members
            history_item['computed_money'] = -share.money / nb_members
        return history_item

    history = list(map(__process_history_item, _select_shares(grappe, user)))

    return history


def _compute_account(grappe, user):
    """
    Compute the account

    :param grappe:
    :param user:
    :return:
    """

    history = build_history(grappe, user)

    return {'money': sum(d['computed_money'] for d in history),
            'time': sum(d['computed_time'] for d in history)}


def compute_accounts(grappe, *users):
    """
    Compute the accounts of users in a given grappe

    :param grappe:
    :param users:
    :return:
    """

    results = list()

    for u in users:
        r = {'user': u}
        r.update(_compute_account(grappe, u))
        results.append(r)

    return results


def compute_html_color(value, min_value, max_value):
    """
    Compute a HTML color given a range
    """
    red = Color("red")
    colors = list(red.range_to(Color("green"), 101))

    if min_value == max_value:
        return "#666666"
    else :
        index = (value - min_value) / (max_value - min_value)
        return colors[int(index * 100)].hex_l


def compute_bounds(grappe):
    """
    Compute the bounds of a grappe
    :param grappe:
    :return: dictionary
    """
    r = list(map(lambda m: _compute_account(grappe, m), grappe.members.all()))

    money_values = list(map(lambda x: x['money'], r))
    min_money = min(money_values)
    max_money = max(money_values)

    time_values = list(map(lambda x: x['time'], r))
    min_time = min(time_values)
    max_time = max(time_values)

    return {"time": {"min": min_time,
                     "max": max_time},
            "money": {"min": min_money,
                      "max": max_money}
            }
