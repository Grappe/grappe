# This file is part of the Grappe project.
# Copyright (C) 2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch.dispatcher import receiver

from grappe_core.models import UserProfile, Resource, GrappeNewsfeed, Share, Idea, Issue, Pool
from grappe_core.compute import compute_bounds


@receiver(post_save)
def add_save_event(sender, instance, created, **kwargs):

    if isinstance(instance, Resource):
        GrappeNewsfeed.add_event(label=f"Resource {'ajoutée' if created else 'mise à jour'}",
                                 text=f"{instance.name}",
                                 pool=instance.pool)

    if isinstance(instance, Share):
        if not instance.from_issue:
            GrappeNewsfeed.add_event(label=f"Échange {'ajouté' if created else 'mis à jour'}",
                                     text=f"{instance.description} ({instance.time} {instance.grappe.time_unit} et {instance.money} {instance.grappe.money_unit})",
                                     grappe=instance.grappe)
        # Update the upper and lower bounds of time/money values whenever a new share is saved.
        bounds = compute_bounds(instance.grappe)
        instance.grappe.min_time_bound = bounds['time']['min']
        instance.grappe.max_time_bound = bounds['time']['max']
        instance.grappe.min_money_bound = bounds['money']['min']
        instance.grappe.max_money_bound = bounds['money']['max']
        instance.grappe.save()

    if isinstance(instance, Idea):
        GrappeNewsfeed.add_event(label=f"Une envie a été {'ajoutée' if created else 'mise à jour'}",
                                 text=f"{instance.member} {instance.description}",
                                 grappe=instance.grappe)

    if isinstance(instance, Issue):
        GrappeNewsfeed.add_event(label=f"Un événement a été {'ajouté' if created else 'mis à jour'}",
                                 text=f"{instance.description}",
                                 pool=instance.pool)

    if isinstance(instance, Pool):
        GrappeNewsfeed.add_event(label=f"Un commun a été {'ajouté' if created else 'mis à jour'}",
                                 text=f"{instance.description}",
                                 pool=instance)

    for class_to_check in GrappeNewsfeed.save_and_update_registered_classes:
        if isinstance(instance, class_to_check):
            if hasattr(instance, "pool"):
                pool = instance.pool
                grappe = pool.grappe
            else:
                pool = None
                grappe = instance.grappe
            GrappeNewsfeed.add_event(label=f"{'' if created else 'Mis(e) à jour : '}",
                                     text=f"{instance}",
                                     pool=pool,
                                     grappe=grappe)

    # To add a user profile
    if isinstance(instance, User):
        if created:
            UserProfile.objects.create(user=instance)
