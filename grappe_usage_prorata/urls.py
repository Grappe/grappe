# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.urls import path
from .views import ObjectUsageCreateView, ObjectUsageUpdateView, ObjectUsageDeleteView, CostCreateView, \
    CostUpdateView, CostDeleteView, ObjectCreateView, ObjectUpdateView, ObjectDeleteView, ObjectDetailView

# Namespace
app_name = __package__

urlpatterns = [

    # ----- Objects -----
    path('<pool>/object/create/', ObjectCreateView.as_view(), name='create_object'),
    path('<pool>/object/<slug>/', ObjectDetailView.as_view(), name='object_details'),
    path('<pool>/object/<slug>/update/', ObjectUpdateView.as_view(), name='update_object'),
    path('<pool>/object/<slug>/delete/', ObjectDeleteView.as_view(), name='delete_object'),

    # ----- The usage of the objects ----
    path('<pool>/usage/create/', ObjectUsageCreateView.as_view(), name='create_usage'),
    path('<pool>/usage/<pk>/update/', ObjectUsageUpdateView.as_view(), name='update_usage'),
    path('<pool>/usage/<pk>/delete/', ObjectUsageDeleteView.as_view(), name='delete_usage'),

    # ---- The costs -----
    path('<pool>/cost/create/', CostCreateView.as_view(), name='create_cost'),
    path('<pool>/cost/<pk>/update/', CostUpdateView.as_view(), name='update_cost'),
    path('<pool>/cost/<pk>/delete/', CostDeleteView.as_view(), name='delete_cost'),

]
