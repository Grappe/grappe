# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


def compute_usage_per_resource(pool):
    from grappe_usage_prorata.models import ObjectUsage, Object
    results = list()
    for resource in Object.objects.filter(pool=pool):
        usages = list()
        for member in pool.members.all():
            r = {'user': member.username,
                 'count': len(ObjectUsage.objects.filter(pool=pool, member=member, resource=resource))
                 }
            usages.append(r)
        results.append({'resource': resource.name,
                        'usages': usages})
    return results


def is_empty_share(share):
    return share['time'] == 0 and share['money'] == 0


def compute_share_per_member(pool, user, time, money):
    from grappe_usage_prorata.models import ObjectUsage
    total_uses = len(ObjectUsage.objects.filter(pool=pool))
    uses = len(ObjectUsage.objects.filter(pool=pool, member=user))
    try:
        return {'time': uses * time / total_uses,
                'money': uses * money / total_uses}
    except Exception:
        return {'time': 0,
                'money': 0}
