# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
from django.test import TestCase
from django.contrib.auth.models import User

from grappe_core.compute import compute_accounts
from grappe_core.models import Grappe
from grappe_core.tests import create_users, create_grappe
from grappe_usage_prorata.models import UsageProrataPool, Cost, Object, ObjectUsage


class UsageProrataPoolTestCase(TestCase):

    def __configure(self):

        self.peter_usage = 3
        self.bob_usage = 10
        self.marie_usage = 20
        self.jeanne_usage = 2
        self.olivier_usage = 1

        self.cost_money = 100
        self.cost_time = 2

    def setUp(self):

        self.__configure()

        users = create_users()
        grappe = create_grappe(users)

        pool = UsageProrataPool(name="En fonction de l'utilisation",
                                description="Pour un partage au prorata de l'utilisation",
                                grappe=grappe)
        pool.save()
        pool.members.set(users)
        pool.save()

        an_object = Object(owner=User.objects.get(username='bob'),
                           name='Eponge',
                           pool=pool)
        an_object.save()

        usages = {'peter': self.peter_usage,
                  'bob': self.bob_usage,
                  'marie': self.marie_usage,
                  'jeanne': self.jeanne_usage,
                  'olivier': self.olivier_usage}

        for name, count in usages.items():
            for k in range(0, count):
                u = ObjectUsage(date=datetime.datetime.now(),
                                pool=pool,
                                member=User.objects.get(username=name),
                                resource=an_object)

                u.save()

        cost = Cost(description="Dépense",
                    date=datetime.datetime.now(),
                    issuer=User.objects.get(username="bob"),
                    money=self.cost_money,
                    time=self.cost_time,
                    pool=pool
                    )
        cost.save()

    @staticmethod
    def __compute_user_cost(usage, total_cost, total_usage):
        return usage * total_cost / total_usage

    def test_accounts(self):
        """Validate the accounts"""
        grappe = Grappe.objects.get(name='Les Volontaires')
        accounts = compute_accounts(grappe, *grappe.members.all())

        total_usages = self.peter_usage + self.bob_usage + self.marie_usage + self.jeanne_usage + self.olivier_usage

        expected_results = [
            {'user': User.objects.get(username='bob'),
             'time': self.cost_time - self.__compute_user_cost(self.bob_usage, self.cost_time, total_usages),
             'money': self.cost_money - self.__compute_user_cost(self.bob_usage, self.cost_money, total_usages)},
            {'user': User.objects.get(username='peter'),
             'time': - self.__compute_user_cost(self.peter_usage, self.cost_time, total_usages),
             'money': - self.__compute_user_cost(self.peter_usage, self.cost_money, total_usages)},
            {'user': User.objects.get(username='julie'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='marie'),
             'time': - self.__compute_user_cost(self.marie_usage, self.cost_time, total_usages),
             'money': - self.__compute_user_cost(self.marie_usage, self.cost_money, total_usages)},
            {'user': User.objects.get(username='jacques'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='pierre'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='olivier'),
             'time': - self.__compute_user_cost(self.olivier_usage, self.cost_time, total_usages),
             'money': - self.__compute_user_cost(self.olivier_usage, self.cost_money, total_usages)},
            {'user': User.objects.get(username='jeanne'),
             'time': - self.__compute_user_cost(self.jeanne_usage, self.cost_time, total_usages),
             'money': - self.__compute_user_cost(self.jeanne_usage, self.cost_money, total_usages)},
            {'user': User.objects.get(username='élodie'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='cécile'),
             'time': 0,
             'money': 0},
        ]

        # FIXME: math problem: the difference is :
        # 1.4444444444444446
        # 1.4444444444444444
        # Should be related to the store into the database. How to handle this problem?
        for k in accounts:
            k['time'] = float(str(k['time'])[:8])
            k['money'] = float(str(k['money'])[:8])
        for k in expected_results:
            k['time'] = float(str(k['time'])[:8])
            k['money'] = float(str(k['money'])[:8])

        self.assertEqual(accounts, expected_results)
