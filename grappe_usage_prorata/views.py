# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView, DeleteView, CreateView, DetailView
from django.urls.base import reverse_lazy

from grappe_ui.views import GrappeView, PoolView, BackToPoolView
from grappe_core.models import Pool

from grappe_usage_prorata.compute import compute_usage_per_resource
from grappe_usage_prorata.forms import UsageForm, CostForm, ObjectModelForm
from grappe_usage_prorata.models import ObjectUsage, Cost, UsageProrataPool, Object

# ~~~~~~~~~~~~~~~~~~ Pool ~~~~~~~~~~~~~~~~~~~~


class PoolDetailView(LoginRequiredMixin, PoolView, DetailView):
    model = UsageProrataPool
    template_name = f'{__package__}/pool.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        pool = kwargs['object']
        context = super().get_context_data(**kwargs)
        context['type'] = UsageProrataPool.verbose_name
        context['usages'] = ObjectUsage.objects.filter(pool=pool).order_by('-date')
        context['costs'] = Cost.objects.filter(pool=pool).order_by('-date')
        context['objects'] = Object.objects.filter(pool=pool)
        context['counts'] = compute_usage_per_resource(pool)
        return context

# ~~~~~~~~~~~~~~~~~~ Object Usage ~~~~~~~~~~~~~~~~~~~~


class ObjectUsageCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = ObjectUsage
    form_class = UsageForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_initial(self):
        initial = super().get_initial()
        initial['pool'] = Pool.objects.get(slug=self.kwargs['pool'])
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Ajouter une utilisation d'objet"
        context['button_label'] = 'Ajouter'
        return context


class ObjectUsageUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = ObjectUsage
    form_class = UsageForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier une utilisation'
        context['button_label'] = 'Modifier'
        return context


class ObjectUsageDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = ObjectUsage
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer une utilisation'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette utilisation '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

# ~~~~~~~~~~~~~~~~~~ Cost ~~~~~~~~~~~~~~~~~~~~


class CostCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = Cost
    form_class = CostForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Ajouter un coût'
        context['button_label'] = 'Ajouter'
        return context


class CostUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = Cost
    form_class = CostForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_initial(self):
        initial = super().get_initial()
        initial['pool'] = Pool.objects.get(slug=self.kwargs['pool'])
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier un coût'
        context['button_label'] = 'Modifier'
        return context


class CostDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = Cost
    form_class = CostForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer un coût'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer ce coût '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

# ~~~~~~~~~~~~~~~~~~ Object ~~~~~~~~~~~~~~~~~~~~


class ObjectCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = Object
    form_class = ObjectModelForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Créer une nouvelle ressource"
        context['button_label'] = 'Ajouter'
        return context


class ObjectDetailView(LoginRequiredMixin, GrappeView, DetailView):
    model = Object
    template_name = f'{__package__}/resource.html'
    login_url = reverse_lazy('login')


class ObjectUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = Object
    form_class = ObjectModelForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier une ressource'
        context['button_label'] = 'Modifier'
        return context


class ObjectDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = Object
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer une ressource'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette ressource: '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context
