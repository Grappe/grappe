# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.forms import ModelForm, Select, TextInput, HiddenInput, Textarea, CheckboxSelectMultiple
from grappe_core.models import Pool, Grappe
from grappe_ui.forms import add_type

from .models import ObjectUsage, Cost, Object, UsageProrataPool

_attrs = {'class': 'form-control'}


class UsageProrataPoolModelForm(ModelForm):

    class Meta:
        model = UsageProrataPool
        fields = ['name', 'members', 'description', 'grappe']
        widgets = {'name': TextInput(attrs=_attrs),
                   'members': CheckboxSelectMultiple(attrs={"class": "column-checkbox members-scroll"}),
                   'description': Textarea(attrs=_attrs),
                   'grappe': HiddenInput()}

    def __init__(self, *args, **kwargs):
        grappe = Grappe.objects.get(slug=kwargs.pop('grappe'))
        super().__init__(*args, **kwargs)

        # Set the query set for the fields
        grappe_members = User.objects.filter(username__in=[m.username for m in grappe.members.all()])
        self.fields['members'].queryset = grappe_members

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['members'].label = "Membres"
        self.fields['description'].label = "Charte"

        # Set the pool, as hidden field
        self.fields['grappe'].initial = grappe


class UsageForm(ModelForm):

    class Meta:
        model = ObjectUsage
        fields = ['member', 'date', 'resource', 'pool']
        widgets = {'member': Select(attrs=_attrs),
                   'date': TextInput(attrs=add_type(_attrs, 'date')),
                   'resource': Select(attrs=_attrs),
                   'pool': HiddenInput()
                   }

    def __init__(self, *args, **kwargs):
        pool = Pool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)
        # Set the labels of the fields
        self.fields['member'].label = "Utilisateur"
        self.fields['member'].queryset = pool.members
        self.fields['date'].label = "Date"
        self.fields['resource'].label = "Ressource"
        self.fields['resource'].queryset = Object.objects.filter(pool=pool)


class CostForm(ModelForm):

    class Meta:
        model = Cost
        fields = ['issuer', 'date', 'description', 'money', 'time', 'pool', 'tag']
        widgets = {'issuer': Select(attrs=_attrs),
                   'date': TextInput(attrs=add_type(_attrs, 'date')),
                   'money': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'description': Textarea(attrs=_attrs),
                   'time': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'pool': HiddenInput(),
                   'tag': TextInput(attrs=_attrs)
                   }

    def __init__(self, *args, **kwargs):
        pool = Pool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)
        self.fields['issuer'].queryset = pool.members

        # Set the labels
        self.fields['issuer'].label = "Membre"
        self.fields['money'].label = f"Coût en argent ({pool.grappe.money_unit})"
        self.fields['time'].label = f"Coût en temps ({pool.grappe.time_unit})"
        self.fields['tag'].label = "Etiquette"

        # Set the pool, as hidden field
        self.fields['pool'].initial = pool


class ObjectModelForm(ModelForm):

    class Meta:
        model = Object
        fields = ['name', 'owner', 'description', 'pool']
        widgets = {'name': TextInput(attrs=_attrs),
                   'owner': Select(attrs=_attrs),
                   'description': Textarea(attrs=_attrs),
                   'pool': HiddenInput()}

    def __init__(self, *args, **kwargs):
        pool = UsageProrataPool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['owner'].label = "Propriétaire"
        self.fields['description'].label = "Description"

        # Set the query set for the members
        self.fields['owner'].queryset = User.objects.filter(username__in=[m.username for m in pool.members.all()])

        # Set the pool, as hidden field
        self.fields['pool'].initial = pool
