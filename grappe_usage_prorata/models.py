# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import uuid
from django.contrib.auth.models import User
from django.db import models
from grappe_core.models import Pool, Issue, Resource
from grappe_usage_prorata.compute import compute_share_per_member, is_empty_share


class UsageProrataPool(Pool):
    verbose_name = "Basé sur l'utilisation"


class Object(Resource):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)


class ObjectUsage(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date = models.DateField()
    pool = models.ForeignKey(Pool, on_delete=models.CASCADE)
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    resource = models.ForeignKey(Object, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.member} a utilisé {self.resource.name}"


class Cost(Issue):

    issuer = models.ForeignKey(User, on_delete=models.CASCADE)
    money = models.FloatField()
    time = models.FloatField()
    tag = models.CharField(max_length=30, blank=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def absorb(self) -> list:
        shares = list()
        for member in self.pool.members.all():
            share_per_member = compute_share_per_member(self.pool, member, self.time, self.money)
            if not is_empty_share(share_per_member):
                shares.append({'from': self.issuer,
                               'to': [member],
                               'time': share_per_member['time'],
                               'money': share_per_member['money'],
                               'tag': self.tag,
                               'date': self.date,
                               'description': self.description})
        return shares
