# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.apps import AppConfig


class GrappeUsageProrataConfig(AppConfig):
    name = 'grappe_usage_prorata'

    def ready(self):
        '''
        Register here your module:
          - your pool model form (the model itself will be automatically registered)
          - your details view class (it will be configured with the correct reference name)

          - [optional because automatically generated] a specific create/update form class
          - [optional because automatically generated] a specific create/update/delete urlpattern
          - [optional because default is form.html] a specific name for your create/update/delete views

        Don't forget to finalize the registration to see your module in the website
        '''

        from grappe_core.module import PoolsRegistry
        from grappe_usage_prorata.forms import UsageProrataPoolModelForm
        from grappe_usage_prorata.views import PoolDetailView

        PoolsRegistry.set_pool_form_class(UsageProrataPoolModelForm)
        PoolsRegistry.set_pool_details_view_class(PoolDetailView)

        # To distinguish create and update form class
        # PoolsRegistry.set_pool_create_form_class(my_specific_create_form_class)
        # PoolsRegistry.set_pool_update_form_class(my_specific_update_form_class)

        # To customize an urlpattern
        # PoolsRegistry.set_pool_[create, update, delete]_urlpattern(path('my_custom_url/', MyCustomView.as_view()))

        # To customize a template name
        # PoolsRegistry.set_pool_[create, update, delete]_templatename('my_template_name.html')

        PoolsRegistry.finalize_registration()
