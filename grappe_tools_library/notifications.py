# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.core.mail import send_mail


def prepare_email(resource_borrow):
    text = f"Une nouvelle demande d'emprunt vient d'être faite pour le commun '{resource_borrow.pool.name}'. \n\n"
    text += f"L'utilisateur '{resource_borrow.borrower.username}' a demandé à emprunter " \
            f"la resource '{resource_borrow.resource.name}' le {str(resource_borrow.date)}"
    text += '\n\n-- Ceci est un mail automatique, merci de ne pas y répondre -- \n'
    text += f"-- Envoyé par Grappe -- "
    return text


def send_notification(resource_borrow):
    try:
        send_mail("[Grappe] Demande d'emprunt",
                  prepare_email(resource_borrow),
                  'no-reply@grappe.org',
                  [resource_borrow.pool.email],
                  fail_silently=False)
    except Exception as e:
        print(f"[Grappe] Cannot send the email: {str(e)}")