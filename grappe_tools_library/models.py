# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import uuid

from django.db import models
from django.db.models import FloatField
from grappe_core.models import Pool, Resource
from grappe_core.utils import get_file_upload_path
from django.contrib.auth.models import User
from grappe_tools_library.notifications import send_notification


class ToolsLibraryPool(Pool):

    module_name = __package__
    verbose_name = 'Bibliothèque de ressources'

    admin = models.ForeignKey(User, on_delete=models.CASCADE)
    email = models.EmailField()


class LibResource(Resource):
    replacement_cost = FloatField()
    photo = models.ImageField(blank=True, upload_to=get_file_upload_path)


class ResourceBorrow(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date = models.DateField()
    borrower = models.ForeignKey(User, on_delete=models.CASCADE)
    resource = models.ForeignKey(LibResource, on_delete=models.CASCADE)
    pool = models.ForeignKey(ToolsLibraryPool, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        send_notification(self)
        super().save(*args, **kwargs)
