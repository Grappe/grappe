# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.urls import path

from grappe_tools_library.views import ResourceCreateView, ResourceUpdateView, ResourceDeleteView, ResourceDetailView, \
    ResourceBorrowCreateView

# Namespace
app_name = __package__

urlpatterns = [

    # ----- Resource ----
    path('<pool>/resource/create', ResourceCreateView.as_view(), name='create_resource'),
    path('<pool>/resource/<slug>/update', ResourceUpdateView.as_view(), name='update_resource'),
    path('<pool>/resource/<slug>', ResourceDetailView.as_view(), name='resource_details'),
    path('<pool>/resource/<slug>/delete', ResourceDeleteView.as_view(), name='delete_resource'),
    path('<pool>/resource/<resource>/borrow', ResourceBorrowCreateView.as_view(), name='borrow_resource'),

]
