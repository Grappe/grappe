# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, UpdateView, DetailView, DeleteView
from django.urls.base import reverse_lazy, reverse
from grappe_ui.views import PoolView, BackToPoolView
from grappe_tools_library.models import LibResource, ToolsLibraryPool, ResourceBorrow
from grappe_tools_library.forms import ResourceModelForm, ToolsLibraryPoolModelForm, ResourceBorrowForm
from grappe_tools_library.apps import GrappeToolsLibraryConfig


class PoolCreateView(LoginRequiredMixin, PoolView, CreateView):
    model = ToolsLibraryPool
    form_class = ToolsLibraryPoolModelForm
    template_name = f'{GrappeToolsLibraryConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        kwargs = super(PoolCreateView, self).get_form_kwargs()
        kwargs['grappe'] = self.kwargs['grappe']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Créer un nouveau commun"
        context['button_label'] = 'Ajouter'
        return context

    def get_success_url(self):
        return reverse(f"{__package__}:pool_details", args=(self.kwargs['grappe'], self.object.slug))


class PoolDetailView(LoginRequiredMixin, PoolView, DetailView):
    model = ToolsLibraryPool
    template_name = f'{GrappeToolsLibraryConfig.name}/pool.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        pool = kwargs['object']
        context = super().get_context_data(**kwargs)
        context['type'] = ToolsLibraryPool.verbose_name
        context['resources'] = LibResource.objects.filter(pool=pool)
        context['admin_view'] = pool.admin == self.request.user
        return context


class ResourceCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = LibResource
    form_class = ResourceModelForm
    template_name = f'{GrappeToolsLibraryConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        kwargs = super(ResourceCreateView, self).get_form_kwargs()
        kwargs['pool'] = self.kwargs['pool']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Ajouter une ressource"
        context['button_label'] = 'Ajouter'
        context['enctype'] = 'multipart/form-data'
        return context


class ResourceUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = LibResource
    form_class = ResourceModelForm
    template_name = f'{GrappeToolsLibraryConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        kwargs = super(ResourceUpdateView, self).get_form_kwargs()
        kwargs['pool'] = self.kwargs['pool']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Modifier une ressource"
        context['button_label'] = 'Modifier'
        context['enctype'] = 'multipart/form-data'
        return context


class ResourceDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = LibResource
    template_name = f'{GrappeToolsLibraryConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Supprimer une ressource"
        context['button_label'] = 'Supprimer'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer la ressource : {str(kwargs['object'])}'?"
        return context


class ResourceDetailView(LoginRequiredMixin, BackToPoolView, DetailView):
    model = LibResource
    template_name = f'{GrappeToolsLibraryConfig.name}/resource.html'
    login_url = reverse_lazy('login')


class ResourceBorrowCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = ResourceBorrow
    form_class = ResourceBorrowForm
    template_name = f'{GrappeToolsLibraryConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        kwargs = super(ResourceBorrowCreateView, self).get_form_kwargs()
        kwargs['pool'] = self.kwargs['pool']
        kwargs['resource'] = self.kwargs['resource']
        kwargs['borrower'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Emprunter une ressource "
        context['button_label'] = 'Faire une demande'
        resource = LibResource.objects.get(slug=self.kwargs['resource'])
        context['paragraph'] = f"Êtes vous sûr de vouloir emprunter la ressource '{resource.name}'?"
        return context
