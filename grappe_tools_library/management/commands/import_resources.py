# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from os import listdir
from os.path import isfile, join
from django.core.files import File
from django.core.management.base import BaseCommand
from grappe_tools_library.models import ToolsLibraryPool, LibResource


class Command(BaseCommand):
    help = 'To import resources into a OneWayLib pool'

    def add_arguments(self, parser):
        parser.add_argument('folder_path', type=str, help='Path of the folder containing the photos')
        parser.add_argument('pool_slug', type=str, help='The pool slug in which it should be added')

    def handle(self, *args, **options):
        pool = ToolsLibraryPool.objects.get(slug=options['pool_slug'])
        folder = options['folder_path']
        for k in [f for f in listdir(folder) if isfile(join(folder, f))]:
            r = LibResource(name=k,
                            description='Description of the tool',
                            replacement_cost=1,
                            pool=pool)
            r.save()
            r.photo.save(k, File(open(join(folder,k), 'rb')))
