# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from datetime import datetime

from django.forms import ModelForm, HiddenInput, TextInput, Textarea, CheckboxSelectMultiple, FileInput, Select
from django.contrib.auth.models import User
from grappe_core.models import Grappe
from grappe_tools_library.models import LibResource, ToolsLibraryPool, ResourceBorrow

_attrs = {'class': 'form-control'}


class ToolsLibraryPoolModelForm(ModelForm):

    class Meta:
        model = ToolsLibraryPool
        fields = ['name', 'members', 'admin', 'email', 'description', 'grappe']
        widgets = {'name': TextInput(attrs=_attrs),
                   'admin': Select(attrs=_attrs),
                   'email': TextInput(attrs=_attrs),
                   'members': CheckboxSelectMultiple(attrs={"class": "column-checkbox members-scroll"}),
                   'description': Textarea(attrs=_attrs),
                   'grappe': HiddenInput()}

    def __init__(self, *args, **kwargs):
        grappe = Grappe.objects.get(slug=kwargs.pop('grappe'))
        super(ToolsLibraryPoolModelForm, self).__init__(*args, **kwargs)

        # Set the query set for the fields
        grappe_members = User.objects.filter(username__in=[m.username for m in grappe.members.all()])
        self.fields['members'].queryset = grappe_members

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['members'].label = "Membres"
        self.fields['description'].label = "Charte"
        self.fields['admin'].label = "Administrateur"
        self.fields['email'].label = "Email"

        # Set the pool, as hidden field
        self.fields['grappe'].initial = grappe


class ResourceModelForm(ModelForm):

    class Meta:
        model = LibResource
        fields = ['name', 'description', 'replacement_cost', 'tag', 'photo', 'pool']
        widgets = {'name': TextInput(attrs=_attrs),
                   'description': Textarea(attrs=_attrs),
                   'replacement_cost': TextInput(attrs=_attrs),
                   'tag': TextInput(attrs=_attrs),
                   'photo': FileInput(attrs=_attrs),
                   'pool': HiddenInput()}

    def __init__(self, *args, **kwargs):
        pool = ToolsLibraryPool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['description'].label = "Description"
        self.fields['replacement_cost'].label = f"Valeur de remplacement (en {pool.grappe.money_unit})"
        self.fields['tag'].label = "Etiquette"
        self.fields['photo'].label = "Photo"

        # Set the pool, as hidden field
        self.fields['pool'].initial = pool


class ResourceBorrowForm(ModelForm):

    class Meta:
        model = ResourceBorrow
        fields = ['date', 'borrower', 'resource', 'pool']
        widgets = {'date': HiddenInput(),
                   'borrower': HiddenInput(),
                   'resource': HiddenInput(),
                   'pool': HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        pool = ToolsLibraryPool.objects.get(slug=kwargs.pop('pool'))
        resource = LibResource.objects.get(slug=kwargs.pop('resource'))
        borrower = kwargs.pop('borrower')
        super().__init__(*args, **kwargs)

        # Set the hidden fields
        self.fields['date'].initial = datetime.now().strftime("%Y-%m-%d")
        self.fields['borrower'].initial = borrower
        self.fields['resource'].initial = resource
        self.fields['pool'].initial = pool
