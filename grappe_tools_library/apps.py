# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.apps import AppConfig


class GrappeToolsLibraryConfig(AppConfig):
    name = 'grappe_tools_library'

    def ready(self):

        from grappe_core.module import PoolsRegistry
        from grappe_tools_library.forms import ToolsLibraryPoolModelForm
        from grappe_tools_library.views import PoolDetailView

        PoolsRegistry.set_pool_form_class(ToolsLibraryPoolModelForm)
        PoolsRegistry.set_pool_details_view_class(PoolDetailView)

        PoolsRegistry.finalize_registration()
