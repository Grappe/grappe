Grappe
=========

A web application to help each others in daily life :-)

Forum : https://grappe.flarum.cloud/

Prerequisites
--------------
- Linux
- Python >=3.6 (see https://docs.djangoproject.com/en/3.0/faq/install/) because the application has been writing in Django 3.0.
- PIP (``sudo apt-get install python3-pip``)
- The virtual environment package (venv): ``python3 -m pip install virtualenv`` or ``pip3 install virtualenv``
- git
- bootstrap 
- django 
- mysqlclient (in case of MySQL database)

Installation
--------------
- Download the code of Grappe: 
    - ``cd`` into the folder where you want Grappe (for example in /var/www/)
    - ``git clone https://framagit.org/Grappe/grappe.git`` (will create a new directory ``grappe``)
- cd into ``grappe``
- Set-up your configuration file: 
    - Copy the provided example ``template.config.ini`` into ``config.ini`` in the same folder. 
    - Fill the different fields of your file
        - create the static and media folder - make sure they exist !
- Install the application: 
    - ``./install.sh folder/where/venv/shoud/be/created`` (be sure, you are with a account that can create the directory, and same account where you install the python stuff)
- Run ``python3 manage.py runserver``
- Go to your site !

<details><summary>Optional for now</summary>

- Set-up the web server 
    - [APACHE] 
        - Modify the generated config if necessary
        - Activate your site : ``a2esite yourconf.conf``
        - Reload the Apache web server : ``service apache2 reload``
    - [NGINX]
        - I don't know yet. 
- Set-up the WSGI web server: 
    - Run ``python3 gunicorn -b localhost:1234`` or ``python3 manage.py runserver``

</details>

Administration
----------------

All the administration commands are through ``manage.py``:
- There are the usual Django commands
- And some commands that are Grappe-specific.
    - ``checkconfig``
    - ``filldb``
    - ``genapacheconf``
    - ``getgitcommit``
    - ``install``
    - ``setsecret``
    - ``update``

To get the list of all commands: ``python manage.py help``

Develop a pool type and connect it to Grappe
--------------------------------------------

- The pool details view must inherit grappe_ui.views.PoolView
- The views that needs a success url to redirect to the pool details view can inherit grappe_ui.views.BackToPoolView
- In ``urls.py``
    - You need to have ``app_name = __package__`` to set the namespace
    - You don't need the create, update, details, and delete urlpatterns for the pools views, as they are generated at your pool registration (last step)
    - Give a name to your urlpatterns to refer to it in your templates and/or python code
- In the html templates of all your views, you can refer to your module urls as
    - ``{% url module_name|add:':my_url_name' arg1 arg2... %}`` (module_name is set by grappe_ui.views.PoolView or grappe_ui.views.BackToPoolView)
    - The names for the create, update, details, and delete urls are respectively
        - create_pool
        - update_pool
        - pool_details
        - delete_pool
- All your models must be writen in the ``models.py`` file of your module
- All your views must be writen in the ``views.py`` file of your module
- In ``apps.py``
    - Register your module in the ``ready()`` method (see example in ``grappe/grappe_pot/apps.py``)
    - To be noted: the pool imports are made in the ``ready()`` method
- If you want to see a save, update, and delete event of a custom model in the Grappe Feeds View you can
  decorate you model class with ``@GrappeNewsfeed.save_update_delete_events`` and you must have an attribute
  named ``grappe`` or ``pool`` that references the associated grappe or pool

ToDo 
----
- MODULE
    - implement the modules for the grappe-beta *Digne*
    
- CORE
    - what about setting a configuration object for each grappe ? Or the config in the Grappe object it self ? 
    - how to manage the uploaded file, rename the files to avoid conflicts
    - make color cursors instead of raw numbers
    - check if the user has access to the data relative to a grappe
    
- GUI 
    - think about an intuitive way of using the tool
    - put some logos in the GUI to make it more friendly
    - make the login and registration more friendly
    
- INSTALL
    - install faire un check des prerequisites ! 


Idées
-----
- faire une limitation T/M pour éviter que qqn donne trop et permettre d'équilibrer la grappe. Eviter les déséquilibres. 
- comment faire pour tenir à jour un compte en pouvant éditer les échanges. Il faut les valider à un moment donné ?
- faire un système de mise en relation des gens pour créer des grappes.
