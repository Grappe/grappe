# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Grappe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

from grappe_ui import views as core_views

urlpatterns = [
    # Welcome
    path('', core_views.welcome, name='index'),

    # Login
    path('', include('django.contrib.auth.urls'), name='login'),

    # Admin
    path('admin/', admin.site.urls),

    # To register
    path('register/', core_views.register, name='signup'),

    # Profile of the user
    path('profile/', core_views.UserView.as_view(), name='profile'),
    path('profile/update/', core_views.UserUpdateView.as_view(), name='update_profile'),

    # Grappe user interface
    path('', include('grappe_ui.urls')),

]

# The development server in DEBUG mode does not serve the media files, so activate it.
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
