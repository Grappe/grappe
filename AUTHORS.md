Authors
=======

Grappe is provided thanks to the work of the following contributors:

* [Christophe Lucarz (Outils-Conviviaux)](https://www.outils-conviviaux.fr)
* [L'Atelier-Partagé](http://www.atelier-partage.org)
* Hugo VIRICEL