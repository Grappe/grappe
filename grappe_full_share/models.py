# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.db import models
from grappe_core.models import User, Pool, Issue, Resource

# This is the entry point of the module, a class that should inherit from grappe_core.models.Pool and implement the
# required class methods.


class FullSharePool(Pool):

    max_price_of_resource = models.FloatField()

    module_name = __package__
    verbose_name = 'Partage total'


# These are the models of this module
class CommonResource(Resource):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)


class Expense(Issue):

    issuer = models.ForeignKey(User, on_delete=models.CASCADE)
    money = models.FloatField()
    time = models.FloatField()
    tag = models.CharField(max_length=30, blank=True)

    def absorb(self) -> list:
        return [{'from': self.issuer,
                 'to': self.pool.members.all(),
                 'time': self.time,
                 'money': self.money,
                 'tag': 'Resolution',
                 'date': self.date,
                 'description': self.description}]
