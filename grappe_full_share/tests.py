# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
from django.test import TestCase
from django.contrib.auth.models import User

from grappe_core.compute import compute_accounts
from grappe_core.models import Grappe
from grappe_core.tests import create_users, create_grappe
from grappe_full_share.models import FullSharePool, Expense


class FullSharePoolTestCase(TestCase):
    def setUp(self):
        users = create_users()
        grappe = create_grappe(users)

        pool = FullSharePool(name="Partage total",
                      description="Pour un partage total des ressources",
                      max_price_of_resource = 100,
                      grappe=grappe)
        pool.save()
        pool.members.set(users)
        pool.save()

        expense = Expense(description="Dépense",
                          date=datetime.datetime.now(),
                          issuer=User.objects.get(username="bob"),
                          money=100,
                          time=1,
                          pool=pool
                          )
        expense.save()

    def test_accounts(self):
        """Validate the accounts"""
        grappe = Grappe.objects.get(name='Les Volontaires')
        accounts = compute_accounts(grappe, *grappe.members.all())

        expected_results = [
            {'user': User.objects.get(username='bob'),
             'time': 1 - 1/10,
             'money': 100 - 100/10},
            {'user': User.objects.get(username='peter'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='julie'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='marie'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='jacques'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='pierre'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='olivier'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='jeanne'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='élodie'),
             'time': -1/10,
             'money': -100/10},
            {'user': User.objects.get(username='cécile'),
             'time': -1/10,
             'money': -100/10},
        ]

        self.assertEqual(accounts, expected_results)
