# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.urls import path

from grappe_full_share.views import (ExpenseCreateView, ExpenseUpdateView, ExpenseDeleteView,
                                     CommonResourceCreateView, CommonResourceUpdateView, CommonResourceDeleteView, CommonResourceDetailView)

# Namespace
app_name = __package__

urlpatterns = [

    # ----- Expenses ----
    path('<pool>/expenses/create/', ExpenseCreateView.as_view(), name='create_expense'),
    path('<pool>/expenses/<pk>/update/', ExpenseUpdateView.as_view(), name='update_expense'),
    path('<pool>/expenses/<pk>/delete', ExpenseDeleteView.as_view(), name='delete_expense'),

    # ----- Resources ----
    path('<pool>/resources/create', CommonResourceCreateView.as_view(), name='create_resource'),
    path('<pool>/resources/<slug>/', CommonResourceDetailView.as_view(), name='resource_details'),
    path('<pool>/resources/<slug>/update/', CommonResourceUpdateView.as_view(), name='update_resource'),
    path('<pool>/resources/<slug>/delete/', CommonResourceDeleteView.as_view(), name='delete_resource'),
]
