# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.forms import ModelForm, HiddenInput, Select, TextInput, Textarea, CheckboxSelectMultiple
from django.contrib.auth.models import User

from grappe_core.models import Grappe
from grappe_ui.forms import add_type
from grappe_full_share.models import Expense, CommonResource, FullSharePool

_attrs = {'class': 'form-control'}


class FullSharePoolModelForm(ModelForm):

    class Meta:
        model = FullSharePool
        fields = ['name', 'members', 'description', 'max_price_of_resource', 'grappe']
        widgets = {'name': TextInput(attrs=_attrs),
                   'members': CheckboxSelectMultiple(attrs={"class": "column-checkbox members-scroll"}),
                   'description': Textarea(attrs=_attrs),
                   'max_price_of_resource': TextInput(attrs=_attrs),
                   'grappe': HiddenInput()}

    def __init__(self, *args, **kwargs):
        grappe = Grappe.objects.get(slug=kwargs.pop('grappe'))
        super().__init__(*args, **kwargs)

        # Set the query set for the fields
        grappe_members = User.objects.filter(username__in=[m.username for m in grappe.members.all()])
        self.fields['members'].queryset = grappe_members

        # # Select only the resources that does not belong already to a pool
        # unused_resources = CommonResource.objects.filter(grappe=grappe).exclude(pool__in=FullSharePool.objects.filter(grappe=grappe))
        # if viewtype == 'update':
        #     pool = kwargs['instance']
        #     pool_resources = CommonResource.objects.filter(grappe=grappe).filter(pool__in=[pool])
        #     # TODO: Do not display in the list of the resources the resources that do not belong to the members
        #     #       of this pool. But the selection of the members is done in this form... Should create a dynamic form?
        #     self.fields['resources'].queryset = unused_resources | pool_resources
        # else:
        #     self.fields['resources'].queryset = unused_resources

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['members'].label = "Membres"
        self.fields['description'].label = "Charte"
        self.fields['max_price_of_resource'].label = "Prix max des ressources y figurant"

        # Set the pool, as hidden field
        self.fields['grappe'].initial = grappe


class CommonResourceModelForm(ModelForm):

    class Meta:
        model = CommonResource
        fields = ['name', 'owner', 'description', 'pool']
        widgets = {'name': TextInput(attrs=_attrs),
                   'owner': Select(attrs=_attrs),
                   'description': Textarea(attrs=_attrs),
                   'pool': HiddenInput()}

    def __init__(self, *args, **kwargs):
        pool_slug = kwargs.pop('pool')
        super().__init__(*args, **kwargs)

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['owner'].label = "Propriétaire"
        self.fields['description'].label = "Description"

        # Set the query set for the members
        pool = FullSharePool.objects.get(slug=pool_slug)
        self.fields['owner'].queryset = User.objects.filter(username__in=[m.username for m in pool.members.all()])

        # Set the pool, as hidden field
        self.fields['pool'].initial = pool


class ExpenseModelForm(ModelForm):

    class Meta:
        model = Expense
        fields = ['description', 'date', 'issuer', 'pool', 'money', 'time', 'tag']
        widgets = {'description': TextInput(attrs=_attrs),
                   'date': TextInput(attrs=add_type(_attrs, 'date')),
                   'issuer': Select(attrs=_attrs),
                   'pool': HiddenInput(),
                   'money': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'time': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'tag': TextInput(attrs=_attrs), }

    def __init__(self, *args, **kwargs):
        pool = FullSharePool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)
        # Set the query set for the fields
        pool_members = User.objects.filter(username__in=[m.username for m in pool.members.all()])
        self.fields['issuer'].queryset = pool_members
        # Set the labels of the fields
        self.fields['description'].label = "Description"
        self.fields['issuer'].label = "Qui ?"
        self.fields['money'].label = f"Coût en argent ({pool.grappe.money_unit})"
        self.fields['time'].label = f"Coût en temps ({pool.grappe.time_unit})"
        self.fields['tag'].label = "Etiquette"

        # Set the hidden field
        self.fields['pool'].initial = pool
