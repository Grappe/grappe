# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DeleteView, UpdateView, DetailView, CreateView
from django.urls.base import reverse_lazy, reverse

from grappe_ui.views import GrappeView, BackToPoolView, PoolView

from grappe_full_share.forms import ExpenseModelForm, FullSharePoolModelForm, CommonResourceModelForm
from grappe_full_share.models import Expense, FullSharePool, CommonResource
from grappe_full_share.apps import GrappeFullShareConfig

# ~~~~~~~~~~~~~~~~~~ Pool ~~~~~~~~~~~~~~~~~~~~


class PoolCreateView(LoginRequiredMixin, PoolView, CreateView):
    model = FullSharePool
    form_class = FullSharePoolModelForm
    template_name = f'{GrappeFullShareConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['grappe'] = self.get_grappe_slug()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Créer un nouveau commun de type " + FullSharePool.verbose_name
        context['button_label'] = 'Ajouter'
        return context

    def get_success_url(self):
        return reverse(f"{__package__}:pool_details", args=(self.kwargs['grappe'], self.object.slug))


class PoolDetailView(LoginRequiredMixin, PoolView, DetailView):
    model = FullSharePool
    template_name = f'{GrappeFullShareConfig.name}/pool.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = FullSharePool.verbose_name
        context['expenses'] = Expense.objects.filter(pool=self.object)
        context['resources'] = CommonResource.objects.filter(pool=self.object)
        return context

# ~~~~~~~~~~~~~~~~~~ Resource ~~~~~~~~~~~~~~~~~~~~


class CommonResourceCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = CommonResource
    form_class = CommonResourceModelForm
    template_name = f'{GrappeFullShareConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Créer une nouvelle ressource"
        context['button_label'] = 'Ajouter'
        return context



class CommonResourceDetailView(LoginRequiredMixin, GrappeView, DetailView):
    model = CommonResource
    template_name = f'{GrappeFullShareConfig.name}/resource.html'
    login_url = reverse_lazy('login')


class CommonResourceUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = CommonResource
    form_class = CommonResourceModelForm
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier une ressource'
        context['button_label'] = 'Modifier'
        return context



class CommonResourceDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = CommonResource
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer une ressource'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette ressource: '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

# ~~~~~~~~~~~~~~~~~~ Expense ~~~~~~~~~~~~~~~~~~~~


class ExpenseCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = Expense
    form_class = ExpenseModelForm
    template_name = f'{GrappeFullShareConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Créer une nouvelle dépense"
        context['button_label'] = 'Ajouter'
        return context


class ExpenseUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = Expense
    template_name = f'{GrappeFullShareConfig.name}/form.html'
    form_class = ExpenseModelForm
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier une dépense'
        context['button_label'] = 'Modifier'
        return context


class ExpenseDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = Expense
    template_name = f'{GrappeFullShareConfig.name}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer une dépense'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette dépense '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

