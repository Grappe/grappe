# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from io import StringIO
from django.core.management.base import BaseCommand
from django.core import management
from django.db import connections
from django.db.migrations.loader import MigrationLoader


class Command(BaseCommand):
    help = '*** Update Grappe to a new version ***'

    def add_arguments(self, parser):
        parser.add_argument(
            '--dev',
            type=str,
            help='To update the database not related to a git commit',
        )

    def handle(self, *args, **options):

        dev_migration_name = options['dev']
        if dev_migration_name:
            # If --dev is specified, take the given name
            migration_name = dev_migration_name[0]
        else:
            # Otherwise take the git commit
            out = StringIO()
            management.call_command('getgitcommit', stdout=out)
            migration_name = out.getvalue()

        # Make migrations
        management.call_command('makemigrations', 'grappe_core', verbosity=2, name=migration_name)

        # Search for the migration file
        migrationfile = ''
        loader = MigrationLoader(connections['default'])
        loader.load_disk()
        for migkey in loader.disk_migrations.keys():
            if migkey[0] == 'grappe_core' and migration_name in migkey[1]:
                migrationfile = migkey[1]

        if migrationfile:
            # SQL migrate
            management.call_command('sqlmigrate', 'grappe_core', migrationfile, verbosity=2)

            # Migrate
            management.call_command('migrate', verbosity=2)
