# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import urllib
from django.core.management.base import BaseCommand
from urllib.request import Request
from random import randint, sample

from grappe_core.models import User, Share, Grappe, Idea
from grappe_full_share.models import FullSharePool, Expense, CommonResource
from grappe_usage_prorata.models import UsageProrataPool, Object, ObjectUsage, Cost


class Command(BaseCommand):
    help = 'Fill database for tests'
    nb_users = 30
    nb_grappes = 10
    words = list()

    def add_arguments(self, parser):
        pass

    def __gen_description(self):
        return "Pour faire {0} avec {1}".format(self.__pick_name(), self.__pick_name())

    def __gen_words(self):
        word_url = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"
        req = Request(word_url, headers={'User-Agent': 'Mozilla/5.0'})
        response = urllib.request.urlopen(req)
        long_txt = response.read().decode()
        self.words = long_txt.splitlines()

    def __pick_name(self):
        return sample(self.words, 1)[0]

    def __pick_members_of_grappe(self, grappe, n):
        if n == 1:
            return sample(list(grappe.members.all()), 1)[0]
        else:
            return sample(list(grappe.members.all()), n)

    def __gen_grappe_name(self):
        return "{0} of {1}".format(self.__pick_name(), self.__pick_name())

    def __gen_set_of_users(self):
        users = list()
        for i in sample(range(1, self.nb_users), randint(5, 10)):
            u = User.objects.all()[i]
            users.append(u)
        if User.objects.filter(username='admin').exists():
            users.append(User.objects.get(username='admin'))  # Add the admin user in order to see all the data
        else:
            raise Exception("Create a user called 'admin' first")
        return users

    def set_users(self):
        for i in range(0, self.nb_users):
            user = User.objects.create_user(self.__pick_name(), password='az04.')
            user.is_superuser = False
            user.is_staff = False
            user.save()

    def set_grappes(self):
        grappes = list()
        for i in range(0, self.nb_grappes):
            g = Grappe(name=self.__gen_grappe_name())
            g.save()
            g.members.set(self.__gen_set_of_users())
            grappes.append(g)
        return grappes

    def set_shares(self, grappes):
        for grappe in grappes:
            for _ in range(0, 10):
                Share.objects.create_share(self.__pick_members_of_grappe(grappe, 1),
                                           self.__pick_members_of_grappe(grappe,
                                                                         randint(2, len(list(grappe.members.all())))),
                                           randint(1, 100),
                                           randint(1, 100),
                                           self.__pick_name(),
                                           datetime.datetime.now(),
                                           self.__gen_description(),
                                           grappe)

    def set_fullshare_pool(self, grappe):
        # Create the pool
        p = FullSharePool(name=self.__pick_name(),
                          description="On met tout en commun avec des règles",
                          grappe=grappe,
                          max_price_of_resource=randint(1, 100)
                          )
        p.save()
        p.members.set(grappe.members.all())

        # Create some resources
        for i in range(0, 2):
            r = CommonResource(name=self.__pick_name(),
                         owner=self.__pick_members_of_grappe(grappe, 1),
                         description='This is a resource',
                         pool=p)
            r.save()

        # Create some expenses
        for j in range(0, 3):
            e = Expense(description='This is an expense',
                        date='2020-01-01',
                        pool=p,
                        tag='label-test',
                        issuer=self.__pick_members_of_grappe(grappe, 1),
                        money=randint(1, 100),
                        time=randint(1, 100))
            e.save()

        return p

    def set_usage_prorata_pool(self, grappe):
        # Create the pool
        p = UsageProrataPool(name=self.__pick_name(),
                             description="En fonction de l'utilisation",
                             grappe=grappe,
                             )
        p.save()
        p.members.set(grappe.members.all())

        # Create some objects
        for i in range(0, 2):
            o = Object(name=self.__pick_name(),
                       owner=self.__pick_members_of_grappe(grappe, 1),
                       description='This is an object',
                       pool=p)
            o.save()

            # Create some object usages
            for j in range(0, 3):
                u = ObjectUsage(date='2020-01-01',
                                pool=p,
                                member=self.__pick_members_of_grappe(grappe, 1),
                                resource=o,
                                )
                u.save()

        # Create some costs
        for k in range(0, 2):
            c = Cost(description='This is a cost',
                     date='2020-01-01',
                     pool=p,
                     tag='label-test',
                     issuer=self.__pick_members_of_grappe(grappe, 1),
                     money=randint(1, 100),
                     time=randint(1, 100))
            c.save()

        return p

    def set_ideas(self, grappes):
        for g in grappes:
            for n in range(0,10):
                idea = Idea(member=self.__pick_members_of_grappe(g, 1),
                            description=f"a envie de {self.__pick_name()}",
                            grappe=g)
                idea.save()

    def handle(self, *args, **options):
        self.__gen_words()
        self.set_users()
        grappes = self.set_grappes()
        for g in grappes:
            self.set_fullshare_pool(g)
            self.set_usage_prorata_pool(g)
        self.set_shares(grappes)
        self.set_ideas(grappes)


