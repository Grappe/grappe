# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from django.core.management.base import BaseCommand
from django.core.management.utils import get_random_secret_key


class Command(BaseCommand):
    help = 'Set the secret in the Grappe configuration file'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        from settings import BASE_DIR
        configfile = os.path.join(BASE_DIR, 'config.ini')

        # Read in the file
        with open(configfile, 'r') as file:
            filedata = file.read()

        # Modify the secret
        filedata = filedata.replace('DO NOT FILL MANUALLY', get_random_secret_key())

        # Write back to the config file
        with open(configfile, 'w') as file:
            file.write(filedata)
