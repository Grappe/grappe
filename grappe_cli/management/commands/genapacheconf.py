# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Generate an Apache .conf for Grappe'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        from settings import CONFIG

        # Read in the file
        with open('apache_template.conf', 'r') as file:
            filedata = file.read()

        # Replace the target string
        filedata = filedata.replace('{{site}}', os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        filedata = filedata.replace('{{media}}', CONFIG['DATA']['media_root'])
        filedata = filedata.replace('{{static}}', CONFIG['DATA']['static_root'])

        # Write the file out again
        with open(CONFIG['INSTALLATION']['apache_conf'], 'w') as file:
            file.write(filedata)

        print('File .conf written: ' + CONFIG['INSTALLATION']['apache_conf'])

        # TODO: review the apache config ...
        # see https://www.metaltoad.com/blog/hosting-django-sites-apache
        # https://stackoverflow.com/questions/6454564/target-wsgi-script-cannot-be-loaded-as-python-module/28118284#28118284

"""
mod_wsgi (pid=15065): Target WSGI script '/var/www/grappe/Grappe/wsgi.py' cannot be loaded as Python module.
[Wed Feb 19 20:21:13.453666 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900] mod_wsgi (pid=15065): Exception occurred processing WSGI script '/var/www/grappe/Grappe/wsgi.py'.
[Wed Feb 19 20:21:13.453695 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900] Traceback (most recent call last):
[Wed Feb 19 20:21:13.453756 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900]   File "/var/www/grappe/Grappe/wsgi.py", line 28, in <module>
[Wed Feb 19 20:21:13.453828 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900]     from django.grappe_core.wsgi import get_wsgi_application
[Wed Feb 19 20:21:13.453854 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900]   File "/var/www/grappe/venv/lib/python3.5/site-packages/django/__init__.py", line 1, in <module>
[Wed Feb 19 20:21:13.453894 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900]     from django.utils.version import get_version
[Wed Feb 19 20:21:13.453917 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900]   File "/var/www/grappe/venv/lib/python3.5/site-packages/django/utils/version.py", line 71, in <module>
[Wed Feb 19 20:21:13.453956 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900]     @functools.lru_cache()
[Wed Feb 19 20:21:13.453992 2020] [wsgi:error] [pid 15065] [remote 78.231.76.127:900] AttributeError: 'module' object has no attribute 'lru_cache'
"""

# seems that apache is not configured for python but for python 2 ... ?
