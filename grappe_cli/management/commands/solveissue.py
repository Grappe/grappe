# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
from django.core.management.base import BaseCommand

from models import Issue


class Command(BaseCommand):
    help = 'solve an issue'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        issue = Issue.objects.get(slug='casse-de-la-remorque')
        issue.solve()
