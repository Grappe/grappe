# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
from django.core import management
import subprocess


class Command(BaseCommand):
    help = 'Installation of Grappe'
    config = None

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        # Generate the apache config
        management.call_command('genapacheconf', verbosity=2)
        # Generate the secret in the config file
        management.call_command('setsecret', verbosity=2)
        # Check the configuration file
        management.call_command('checkconfig', verbosity=2)
        # Check database connection
        management.call_command('checkdbconnection', verbosity=2)
        # Collect static files
        management.call_command('collectstatic', verbosity=2)
        # And update !
        gitversion = subprocess.check_output('git log --first-parent', shell=True)
        management.call_command('update', verbosity=2, dev='commit-' + gitversion[7:15].decode())  # TODO: make the catch of the commit more robust !

        # TODO: if folders in checkconfig not created, then create them !
