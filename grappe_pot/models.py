# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import uuid
from django.contrib.auth.models import User
from django.db import models
from grappe_core.models import Pool, Resource, GrappeNewsfeed


class PotPool(Pool):
    verbose_name = "Cagnotte"


class PotResource(Resource):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    cost_to_pot = models.FloatField()
    cost_to_responsible = models.FloatField()


# > Somebody paid something
@GrappeNewsfeed.save_update_delete_events
class RealPayIssue(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pool = models.ForeignKey(Pool, on_delete=models.CASCADE)
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    cost = models.FloatField()
    date = models.DateField()

    def __str__(self):
        return f'{self.member} a payé {self.cost} avec son argent.'


# > Somebody uses an object
@GrappeNewsfeed.save_update_delete_events
class ResourceUsage(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pool = models.ForeignKey(Pool, on_delete=models.CASCADE)
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    resource = models.ForeignKey(PotResource, on_delete=models.CASCADE)
    cost_to_pot = models.FloatField()
    cost_to_responsible = models.FloatField()
    date = models.DateField()

    def __str__(self):
        return f'{self.member} a utilisé {self.resource}: {self.cost_to_pot} pour la cagnotte, ' \
               f'et {self.cost_to_responsible} pour le responsable.'
