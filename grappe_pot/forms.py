# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.forms import ModelForm, Select, TextInput, HiddenInput, CheckboxSelectMultiple, Textarea
from grappe_core.models import Pool, Grappe
from grappe_pot.models import ResourceUsage, RealPayIssue, PotPool, PotResource
from grappe_ui.forms import add_type

_attrs = {'class': 'form-control'}


class PotPoolModelForm(ModelForm):

    class Meta:
        model = PotPool
        fields = ['name', 'members', 'description', 'grappe']
        widgets = {'name': TextInput(attrs=_attrs),
                   'members': CheckboxSelectMultiple(attrs={"class": "column-checkbox members-scroll"}),
                   'description': Textarea(attrs=_attrs),
                   'grappe': HiddenInput()}

    def __init__(self, *args, **kwargs):
        grappe = Grappe.objects.get(slug=kwargs.pop('grappe'))
        super().__init__(*args, **kwargs)

        # Set the query set for the fields
        grappe_members = User.objects.filter(username__in=[m.username for m in grappe.members.all()])
        self.fields['members'].queryset = grappe_members

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['members'].label = "Membres"
        self.fields['description'].label = "Charte"

        # Set the pool, as hidden field
        self.fields['grappe'].initial = grappe


class ResourceUsageForm(ModelForm):

    class Meta:
        model = ResourceUsage
        fields = ['pool', 'member', 'resource', 'date']
        widgets = {'member': Select(attrs=_attrs),
                   'resource': Select(attrs=_attrs),
                   'date': TextInput(attrs=add_type(_attrs, 'date')),
                   'pool': HiddenInput(),
                   }

    def __init__(self, *args, **kwargs):
        pool = Pool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)

        # Set the query set for the fields
        self.fields['member'].queryset = pool.members
        self.fields['resource'].queryset = PotResource.objects.filter(pool=pool)

        # Set the labels of the fields
        self.fields['member'].label = "Membre"
        self.fields['resource'].label = "Objet"
        self.fields['date'].label = "Date de l'utilisation"

        # Set the hidden field
        self.fields['pool'].initial = pool

    def save(self, commit=True):
        usage = super().save(commit=False)
        resource = self.cleaned_data['resource']
        # Save the current cost to pot and cost to responsible for this given usage:
        usage.cost_to_pot = resource.cost_to_pot
        usage.cost_to_responsible = resource.cost_to_responsible
        if commit:
            usage.save()
        return usage


class RealPayIssueForm(ModelForm):

    class Meta:
        model = RealPayIssue
        fields = ['pool', 'member', 'cost', 'date']
        widgets = {'member': Select(attrs=_attrs),
                   'cost': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'date': TextInput(attrs=add_type(_attrs, 'date')),
                   'pool': HiddenInput()
                   }

    def __init__(self, *args, **kwargs):
        pool = Pool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)

        # Set the query set for the fields
        self.fields['member'].queryset = pool.members

        # Set the labels of the fields
        self.fields['member'].label = "Membre"
        self.fields['cost'].label = f"Montant (en {pool.grappe.money_unit})"
        self.fields['date'].label = "Date"

        # Set the hidden field
        self.fields['pool'].initial = pool


class PotResourceModelForm(ModelForm):

    class Meta:
        model = PotResource
        fields = ['name', 'owner', 'description', 'cost_to_pot', 'cost_to_responsible', 'pool']
        widgets = {'name': TextInput(attrs=_attrs),
                   'owner': Select(attrs=_attrs),
                   'description': Textarea(attrs=_attrs),
                   'cost_to_pot': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'cost_to_responsible': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'pool': HiddenInput()}

    def __init__(self, *args, **kwargs):
        pool = PotPool.objects.get(slug=kwargs.pop('pool'))
        super().__init__(*args, **kwargs)

        # Set the labels of the fields
        self.fields['name'].label = "Nom"
        self.fields['owner'].label = "Propriétaire"
        self.fields['description'].label = "Description"
        self.fields['cost_to_pot'].label = f"Montant pour la cagnotte (en {pool.grappe.money_unit})"
        self.fields['cost_to_responsible'].label = f"Montant pour le responsable (en {pool.grappe.money_unit})"

        # Set the query set for the members
        self.fields['owner'].queryset = User.objects.filter(username__in=[m.username for m in pool.members.all()])

        # Set the pool, as hidden field
        self.fields['pool'].initial = pool
