# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.urls import path

from grappe_pot.views import ResourceUsageCreateView, \
    RealPayCreateView, RealPayUpdateView, ResourceUsageUpdateView, ResourceUsageDeleteView, RealPayDeleteView, \
    PotResourceCreateView, PotResourceUpdateView, PotResourceDeleteView, PotResourceDetailView

# Namespace
app_name = __package__

urlpatterns = [

    # ----- RealPay ----
    path('<pool>/pay/create/', RealPayCreateView.as_view(), name='create_pay'),
    path('<pool>/pay/<pk>/update/', RealPayUpdateView.as_view(), name='update_pay'),
    path('<pool>/pay/<pk>/delete/', RealPayDeleteView.as_view(), name='delete_pay'),

    # ----- Pot Resource ----
    path('<pool>/resources/create/', PotResourceCreateView.as_view(), name='create_resource'),
    path('<pool>/resources/<slug>/', PotResourceDetailView.as_view(), name='resource_details'),
    path('<pool>/resources/<slug>/update/', PotResourceUpdateView.as_view(), name='update_resource'),
    path('<pool>/resources/<slug>/delete/', PotResourceDeleteView.as_view(), name='delete_resource'),

    # ----- ResourceUsage ----
    path('<pool>/usage/create/', ResourceUsageCreateView.as_view(), name='create_usage'),
    path('<pool>/usage/<pk>/update/', ResourceUsageUpdateView.as_view(), name='update_usage'),
    path('<pool>/usage/<pk>/delete/', ResourceUsageDeleteView.as_view(), name='delete_usage'),

]
