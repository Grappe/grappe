# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, UpdateView, DetailView, DeleteView
from grappe_pot.compute import compute_pot_account
from django.urls.base import reverse_lazy

from grappe_pot.forms import ResourceUsageForm, RealPayIssueForm, PotResourceModelForm
from grappe_pot.models import ResourceUsage, RealPayIssue, PotPool, PotResource

from grappe_ui.views import GrappeView, PoolView, BackToPoolView

# ~~~~~~~~~~~~~~~~~~ Pool ~~~~~~~~~~~~~~~~~~~~


class PoolDetailView(LoginRequiredMixin, PoolView, DetailView):
    model = PotPool
    template_name = f'{__package__}/pool.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        pool = kwargs['object']
        context = super().get_context_data(**kwargs)
        context['type'] = PotPool.verbose_name
        context['account'] = compute_pot_account(pool)
        context['resources'] = PotResource.objects.filter(pool=pool)
        context['pays'] = RealPayIssue.objects.filter(pool=pool)
        context['usages'] = ResourceUsage.objects.filter(pool=pool)
        return context

# ~~~~~~~~~~~~~~~~~~ Resource Usage ~~~~~~~~~~~~~~~~~~~~


class ResourceUsageCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = ResourceUsage
    form_class = ResourceUsageForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Ajouter une utilisation d'une ressource"
        context['button_label'] = 'Ajouter'
        return context


class ResourceUsageUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = ResourceUsage
    form_class = ResourceUsageForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Modifier une utilisation d'une ressource"
        context['button_label'] = 'Modifier'
        return context


class ResourceUsageDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = ResourceUsage
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Supprimer une utilisation d'une ressource"
        context['button_label'] = 'Supprimer'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette utilisation : {str(kwargs['object'])}'?"
        return context

# ~~~~~~~~~~~~~~~~~~ Pay ~~~~~~~~~~~~~~~~~~~~


class RealPayCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = RealPayIssue
    form_class = RealPayIssueForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Ajouter un paiement"
        context['button_label'] = 'Ajouter'
        return context


class RealPayUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = RealPayIssue
    form_class = RealPayIssueForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Modifier un paiement"
        context['button_label'] = 'Modifier'
        return context


class RealPayDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = RealPayIssue
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Supprimer un paiement"
        context['button_label'] = 'Supprimer'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer ce paiement : {str(kwargs['object'])}'?"
        return context

# ~~~~~~~~~~~~~~~~~~ Resource ~~~~~~~~~~~~~~~~~~~~


class PotResourceCreateView(LoginRequiredMixin, BackToPoolView, CreateView):
    model = PotResource
    form_class = PotResourceModelForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Créer une nouvelle ressource"
        context['button_label'] = 'Ajouter'
        return context


class PotResourceDetailView(LoginRequiredMixin, GrappeView, DetailView):
    model = PotResource
    template_name = f'{__package__}/resource.html'
    login_url = reverse_lazy('login')


class PotResourceUpdateView(LoginRequiredMixin, BackToPoolView, UpdateView):
    model = PotResource
    form_class = PotResourceModelForm
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier une ressource'
        context['button_label'] = 'Modifier'
        return context


class PotResourceDeleteView(LoginRequiredMixin, BackToPoolView, DeleteView):
    model = PotResource
    template_name = f'{__package__}/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer une ressource'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette ressource: '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context
