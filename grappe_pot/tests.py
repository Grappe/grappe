# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
from django.test import TestCase
from django.contrib.auth.models import User

from grappe_core.compute import compute_accounts
from grappe_core.models import Grappe
from grappe_core.tests import create_users, create_grappe
from grappe_pot.compute import compute_pot_account
from grappe_pot.models import PotPool, PotResource, ResourceUsage, RealPayIssue


class PotPoolTestCase(TestCase):

    def setUp(self):

        self.maxDiff = None

        def __configure():

            self.peter_usage = 3
            self.bob_usage = 10
            self.marie_usage = 20
            self.jeanne_usage = 2
            self.olivier_usage = 1

            self.cost_money = 100

            self.cost_to_pot = 1
            self.cost_to_responsible = 0.75

        __configure()

        self.users = create_users()
        grappe = create_grappe(self.users)

        self.pool = PotPool(name="La cagnotte",
                            description="Une cagnotte à partager",
                            grappe=grappe)
        self.pool.save()
        self.pool.members.set(self.users)
        self.pool.save()

        self.resource = PotResource(owner=User.objects.get(username='bob'),
                                    name='Eponge',
                                    pool=self.pool,
                                    cost_to_pot=self.cost_to_pot,
                                    cost_to_responsible=self.cost_to_responsible)
        self.resource.save()

        usages = {'peter': self.peter_usage,
                  'bob': self.bob_usage,
                  'marie': self.marie_usage,
                  'jeanne': self.jeanne_usage,
                  'olivier': self.olivier_usage}

        for name, count in usages.items():
            for k in range(0, count):
                u = ResourceUsage(date=datetime.datetime.now(),
                                  pool=self.pool,
                                  member=User.objects.get(username=name),
                                  resource=self.resource,
                                  cost_to_pot=self.cost_to_pot,
                                  cost_to_responsible=self.cost_to_responsible)
                u.save()

        pay = RealPayIssue(date=datetime.datetime.now(),
                           member=User.objects.get(username="bob"),
                           cost=self.cost_money,
                           pool=self.pool
                           )
        pay.save()

        self.pot_account = compute_pot_account(self.pool)

    def test_pot_balance(self):
        """ Validate the balance of the pot """

        expected_pot_value = 0

        for usage in ResourceUsage.objects.filter(pool=self.pool, resource=self.resource):
            expected_pot_value += usage.cost_to_pot

        for pay in RealPayIssue.objects.filter(pool=self.pool):
            expected_pot_value -= pay.cost

        self.assertEqual(self.pot_account['balance'], expected_pot_value)

    def test_pot_balance_users(self):
        """ Validate the balance of the pot for each user """

        def __compute_user_balance(user):
            balance = 0

            for pay in RealPayIssue.objects.filter(pool=self.pool, member=user):
                balance += pay.cost

            for resource in PotResource.objects.filter(owner=user):
                for usage in ResourceUsage.objects.filter(pool=self.pool, resource=resource):
                    balance += usage.cost_to_responsible

            for usage in ResourceUsage.objects.filter(pool=self.pool, member=user):
                balance -= usage.cost_to_pot
                balance -= usage.cost_to_responsible

            return balance

        expected_users_balance = [{'user': user,
                                   'balance': __compute_user_balance(user)}
                                  for user in self.users]

        self.assertEqual(self.pot_account['users'], expected_users_balance)

    def test_accounts(self):
        """Validate the accounts"""
        grappe = Grappe.objects.get(name='Les Volontaires')
        accounts = compute_accounts(grappe, *grappe.members.all())

        expected_results = [
            {'user': User.objects.get(username='bob'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='peter'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='julie'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='marie'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='jacques'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='pierre'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='olivier'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='jeanne'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='élodie'),
             'time': 0,
             'money': 0},
            {'user': User.objects.get(username='cécile'),
             'time': 0,
             'money': 0},
        ]

        self.assertEqual(accounts, expected_results)
