# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from grappe_pot.models import ResourceUsage, RealPayIssue
from functools import reduce
from grappe_pot.models import PotResource


def compute_user_account(user, pool):
    """
    Compute the pot account of a user for a given instance of a pool
    :param pool: the pool
    :param user: the user
    :return:
    """
    # Sum the cost_to_pot for each use of resource
    expenses = ResourceUsage.objects.filter(member__username=user.username, pool=pool)
    expenses_sum = reduce(lambda x, y: x + y, map(lambda x: x.cost_to_pot, expenses)) if expenses else 0

    # Sum the pays of this user to the pot
    credits_ = RealPayIssue.objects.filter(member__username=user.username, pool=pool)
    credits_sum = reduce(lambda x, y: x + y, map(lambda x: x.cost, credits_)) if credits_ else 0

    # Sum the cost_to_responsible in case the user is responsible of the resource
    resources_of_the_user = PotResource.objects.filter(owner=user)
    credits_usages = ResourceUsage.objects.filter(pool=pool, resource__in=resources_of_the_user)
    credits_usages_sum = reduce(lambda x, y: x + y, map(lambda x: x.cost_to_responsible, credits_usages)) if credits_usages else 0

    # Sum the cost_to_responsible in case this user is a user of the resource
    expenses_usages = ResourceUsage.objects.filter(member__username=user.username, pool=pool)
    expenses_usages_sum = reduce(lambda x, y: x + y, map(lambda x: x.cost_to_responsible, expenses_usages)) if expenses else 0

    return credits_sum + credits_usages_sum - expenses_sum - expenses_usages_sum


def compute_pot_account(pool):
    """
    Compute the value of the pot among a pool
    :param pool:
    :return:
    """
    users = [{'user': u, 'balance': compute_user_account(u, pool)} for u in pool.members.all()]
    balance = -reduce(lambda x, y: x + y, map(lambda x: x['balance'], users))
    return {'balance': balance, 'users': users}

