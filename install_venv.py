# This file is part of the Grappe project.
# Copyright (C) 2020 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import configparser


def install_venv():

    if not os.path.exists("config.ini"):
        raise Exception('Please create your configuration file config.ini in the same folder as this file.')

    # Create a configuration parser
    config = configparser.RawConfigParser()
    config.read("config.ini")

    # Create a Python virtual environment:
    os.system('virtualenv {0}'.format(config['INSTALLATION']['venv']))

    # Activate the virtual environment
    os.system('source {0}'.format(os.path.join(config['INSTALLATION']['venv'], 'bin', 'activate')))
    # TODO: This source command does not work here !

    # Install the required packages in the virtual environment
    os.system('python3 -m pip install -r requirements.txt')
    # TODO: be careful of the version of python, thus he version of django installed.


if __name__ == '__main__':
    install_venv()
