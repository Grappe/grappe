# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from itertools import chain

from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import DetailView, DeleteView, UpdateView, CreateView
from django.views.generic.list import ListView
from django.db.models import Q
from django.urls.base import reverse_lazy, reverse

from grappe_core.compute import compute_accounts, build_history, compute_html_color
from grappe_core.models import Grappe, Share, Idea, Issue, Resource, UserProfile, GrappeNewsfeed
from grappe_ui.forms import ShareModelForm, GrappeModelForm, AddMemberToGrappeForm, IdeaModelForm, UserProfileModelForm, \
    SignUpForm

from grappe_ui.permissions import has_permission_for_grappe
from grappe_core.module import PoolsRegistry


def update_context_for_grappe_header(context, grappe_slug, request):
    if not 'grappe' in context.keys():
        context['grappe'] = Grappe.objects.get(slug=grappe_slug)
    context['grappes'] = Grappe.objects.filter(Q(members__username=request.user.username))
    return context


class GrappeView(object):

    def get_grappe_slug(self):
        if isinstance(self, GrappeDetailView):
            return self.kwargs['slug']
        else:
            return self.kwargs['grappe']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        grappe_slug = self.get_grappe_slug()
        return update_context_for_grappe_header(context, grappe_slug, self.request)

    def dispatch(self, request, *args, **kwargs):
        grappe_slug = self.get_grappe_slug()
        if not has_permission_for_grappe(request.user, grappe_slug):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class PoolView(GrappeView):
    context_object_name = "pool"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['grappe'] = self.kwargs['grappe']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['module_name'] = self.model.__module__.split(".")[-2]
        return context

    def get_success_url(self):
        module_name = self.model.__module__.split(".")[-2]
        return reverse(f"{module_name}:pool_details", args=(self.kwargs['grappe'], self.object.slug))


class BackToPoolView(GrappeView):

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['pool'] = self.kwargs['pool']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['module_name'] = self.model.__module__.split(".")[-2]
        return context

    def get_success_url(self):
        module_name = self.object.__module__.split(".")[-2]
        return reverse(f"{module_name}:pool_details", args=(self.kwargs['grappe'], self.kwargs["pool"]))


class PoolCreateView(LoginRequiredMixin, PoolView, CreateView):
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f"Créer un nouveau commun de type '{self.model.verbose_name}'"
        context['button_label'] = 'Ajouter'
        return context


class PoolUpdateView(LoginRequiredMixin, PoolView, UpdateView):
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier un commun'
        context['button_label'] = 'Modifier'
        return context


class PoolDeleteView(LoginRequiredMixin, PoolView, DeleteView):
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer un commun'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer ce commun : '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

    def get_success_url(self):
        return reverse("pools", args=(self.kwargs["grappe"],))


def login(request):
    m = User.objects.get(username=request.POST['username'])
    if m.password == request.POST['password']:
        request.session['member_id'] = m.id
        return HttpResponse("You're logged in.")
    else:
        return HttpResponse("Your username and password didn't match.")


def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            # username = form.cleaned_data.get('username')
            # raw_password = form.cleaned_data.get('password1')
            # user = authenticate(username=username, password=raw_password)
            # login(request, user)
            return redirect(reverse_lazy('login'))
    else:
        form = SignUpForm()
    return render(request, 'grappe_ui/register.html', {'form': form})


@login_required(login_url=reverse_lazy('login'))
def welcome(request):
    user = request.user

    def __process_grappe(grappe):
        grappe_dict = dict()
        grappe_dict['grappe'] = grappe

        values = compute_accounts(grappe, user)[0]

        grappe_dict['time'] = values['time']
        grappe_dict['time_color'] = compute_html_color(values['time'], grappe.min_time_bound, grappe.max_time_bound)
        if grappe.min_time_bound != grappe.max_time_bound:
            grappe_dict['time_min'] = grappe.min_time_bound
            grappe_dict['time_max'] = grappe.max_time_bound
        else:
            grappe_dict['time_min'] = -1
            grappe_dict['time_max'] = 1

        grappe_dict['money'] = values['money']
        grappe_dict['money_color'] = compute_html_color(values['money'], grappe.min_money_bound, grappe.max_money_bound)
        if grappe.min_money_bound != grappe.max_money_bound:
            grappe_dict['money_min'] = grappe.min_money_bound
            grappe_dict['money_max'] = grappe.max_money_bound
        else:
            grappe_dict['money_min'] = -1
            grappe_dict['money_max'] = 1

        return grappe_dict

    user_grappes = Grappe.objects.filter(members__username__exact=user.username)
    context = {'grappes': list(map(__process_grappe, user_grappes))}
    return render(request,
                  'grappe_ui/welcome.html',
                  context)


class UserView(LoginRequiredMixin, DetailView):
    model = UserProfile
    template_name = 'grappe_ui/user_profile.html'
    login_url = reverse_lazy('login')

    def get_object(self, queryset=None):
        return UserProfile.objects.get(user=self.request.user)


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = UserProfile
    form_class = UserProfileModelForm
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_object(self, queryset=None):
        return UserProfile.objects.get(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier mon profil'
        context['button_label'] = 'Modifier'
        context['enctype'] = "multipart/form-data"
        return context

    def get_success_url(self):
        return reverse('profile')

# ~~~~~~~~~~~~~~~~~~ Grappe ~~~~~~~~~~~~~~~~~~~~


class GrappeCreateView(LoginRequiredMixin, CreateView):
    form_class = GrappeModelForm
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['creator'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Créer une grappe'
        context['button_label'] = 'Ajouter'
        context['enctype'] = "multipart/form-data"
        return context

    def get_success_url(self):
        return reverse('shares', args=(self.object.slug,))


class GrappeActivityView(LoginRequiredMixin, View):
    template_name = 'grappe_ui/activity.html'
    login_url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        grappe = Grappe.objects.get(slug=self.kwargs['grappe'])
        events = []

        #  Get events with pool
        for module_name, pool_info in PoolsRegistry.pools.items():
            pools = pool_info.model_class.objects.filter(grappe=grappe)
            for pool in pools:
                temp_events = GrappeNewsfeed.objects.filter(pool=pool)
                for event in temp_events:
                    event.module_name = module_name
                    event.pool = pool
                events += temp_events

        #  Get events without pool
        temp_events = GrappeNewsfeed.objects.filter(pool=None, grappe=grappe)
        for event in temp_events:
            event.module_name = ""
        events += temp_events

        activities = sorted(list(map(lambda x: {'description': x.text,
                                                'label': x.label,
                                                'date': x.date,
                                                'module_name': x.module_name,
                                                'pool': x.pool},
                                     events)),
                            key=lambda x: x["date"], reverse=True)
        return render(request,
                      self.template_name,
                      {'activities': activities,
                       'grappe': grappe,
                       'grappes': Grappe.objects.filter(Q(members__username=self.request.user.username))
                      })


class GrappeUpdateView(LoginRequiredMixin, UpdateView, GrappeView):
    model = Grappe
    template_name = 'grappe_ui/form.html'
    form_class = GrappeModelForm
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier une grappe'
        context['button_label'] = 'Modifier'
        context['enctype'] = "multipart/form-data"
        return context

    def get_success_url(self):
        if self.request.user in self.object.members.all():
            return reverse('config_grappe', args=(self.object.slug,))
        else:
            return reverse('index')


class GrappeDeleteView(LoginRequiredMixin, DeleteView, GrappeView):
    model = Grappe
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer une grappe'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette grappe: '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

    def get_success_url(self):
        return reverse('index')


class GrappeDetailView(LoginRequiredMixin, GrappeView, DetailView):
    model = Grappe
    template_name = 'grappe_ui/grappe.html'
    login_url = reverse_lazy('login')


class HistoryView(LoginRequiredMixin, View):
    template_name = 'grappe_ui/history.html'
    login_url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        grappe = Grappe.objects.get(slug=kwargs['slug'])
        context = dict()

        # Update the context for the header
        context = update_context_for_grappe_header(context, kwargs['slug'], request)

        # Build history
        history = build_history(grappe, self.request.user)
        context['history'] = history

        # Compute the time and money totals
        accounts = compute_accounts(grappe, self.request.user)
        context['total_time'] = accounts[0]['time']
        context['total_money'] = accounts[0]['money']

        return render(request, self.template_name, context)

# ~~~~~~~~~~~~~~~~~~ Share ~~~~~~~~~~~~~~~~~~~~


class ShareCreateView(LoginRequiredMixin, GrappeView, CreateView):
    form_class = ShareModelForm
    model = Share
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Ajouter un partage"
        context['button_label'] = 'Ajouter'
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['grappe'] = self.get_grappe_slug()
        return kwargs

    def get_success_url(self):
        return reverse('shares', args=(self.kwargs['grappe'],))


class SharesPerGrappeListView(LoginRequiredMixin, View):
    form_class = ShareModelForm
    template_name = 'grappe_ui/shares_per_grappe.html'
    login_url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        grappe = self.kwargs['grappe']

        # Select the shares that are not included into an Issue
        shares = Share.objects.filter(grappe__slug=grappe).exclude(shares__in=Issue.objects.all())

        # Select the Issues related to this grappe and adding the pool module name as an attribute
        issues = []
        for module_name, pool_info in PoolsRegistry.pools.items():
            temp_issues = Issue.objects.filter(pool__in=pool_info.model_class.objects.filter(grappe=Grappe.objects.get(slug=grappe)))
            for issue in temp_issues:
                issue.module_name = module_name #  Adding an attribute to the issue to have the pool module name
            issues += temp_issues

        context = {'shares': sorted(chain(shares, issues),
                                    key=lambda instance: instance.date,
                                    reverse=True),
                   'grappe': Grappe.objects.get(slug=grappe),
                   }

        # Update the context for the header
        context = update_context_for_grappe_header(context, grappe, request)

        return render(request,
                      self.template_name,
                      context)


class ShareUpdateView(LoginRequiredMixin, GrappeView, UpdateView):
    model = Share
    template_name = 'grappe_ui/form.html'
    form_class = ShareModelForm
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['grappe'] = self.get_grappe_slug()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Modifier un partage'
        context['button_label'] = 'Modifier'
        return context

    def get_success_url(self):
        return reverse('shares', args=(self.kwargs['grappe'],))


class ShareDeleteView(LoginRequiredMixin, GrappeView, DeleteView):
    model = Share
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer un partage'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer ce partage: '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

    def get_success_url(self):
        return reverse('shares', args=(self.kwargs['grappe'],))

# ~~~~~~~~~~~~~~~~~~ Account ~~~~~~~~~~~~~~~~~~~~


class AccountDetailView(LoginRequiredMixin, View):
    template_name = 'grappe_ui/account.html'
    login_url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        grappe_slug = self.kwargs['grappe']
        grappe = Grappe.objects.get(slug=grappe_slug)
        member_username = self.kwargs['username']
        member = User.objects.get(username=member_username)

        member_account = compute_accounts(grappe, member)

        return render(request,
                      self.template_name,
                      {'grappe': Grappe.objects.get(slug=grappe_slug),
                       'shares': Share.objects.filter(Q(grappe=grappe),
                                                      Q(from_member=member) | Q(
                                                          to_members__username__contains=member.username)
                                                      ),
                       'account': member_account,
                       'grappes': Grappe.objects.filter(Q(members=member)),
                       })


class AccountsPerGrappeListView(LoginRequiredMixin, GrappeView, ListView):
    template_name = 'grappe_ui/accounts_per_grappe.html'
    login_url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        grappe_slug = self.kwargs['grappe']
        grappe = Grappe.objects.get(slug=grappe_slug)

        # Compute the accounts
        members_account = compute_accounts(grappe, *grappe.members.all())

        return render(request,
                      self.template_name,
                      {'accounts': members_account,
                       'grappe': grappe,
                       'grappes': Grappe.objects.filter(Q(members__username=self.request.user.username)),
                       })

# ~~~~~~~~~~~~~~~~~~ Idea ~~~~~~~~~~~~~~~~~~~~


class IdeaCreateView(LoginRequiredMixin, View):
    model = Idea
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        grappe = self.kwargs['grappe']

        if 'idea' in self.kwargs:
            idea_slug = self.kwargs['idea']
            idea = Idea.objects.get(slug=idea_slug)

        else:
            idea = None
            idea_slug = ''
        return render(request,
                      self.template_name,
                      {'form': IdeaModelForm(grappe=grappe, instance=idea),
                       'grappe': Grappe.objects.get(slug=grappe),
                       'idea': idea_slug,
                       'title': 'Ajouter une envie',
                       'button_label': 'Ajouter',
                       'grappes': Grappe.objects.filter(Q(members__username=self.request.user.username))
                       })

    def post(self, request, *args, **kwargs):

        grappe = self.kwargs['grappe']

        if 'idea' in self.kwargs:
            idea_slug = self.kwargs['idea']
            idea = Idea.objects.get(slug=idea_slug)
        else:
            idea = None
            idea_slug = ''

        form = IdeaModelForm({'member': request.POST.get('member'),
                              'description': request.POST.get('description'),
                              'grappe': Grappe.objects.get(slug=grappe)},
                             grappe=grappe,
                             instance=idea)
        if form.is_valid():
            form.save() # Create the new idea
        else:
            print('Cannot create the new idea: {0}'.format(form.errors))

        return redirect(reverse('ideas', args=(grappe,)))


class IdeasPerGrappeListView(LoginRequiredMixin, GrappeView, ListView):
    template_name = 'grappe_ui/ideas_per_grappe.html'
    context_object_name = 'ideas'
    login_url = reverse_lazy('login')

    def get_queryset(self):
        grappe = self.get_grappe_slug()
        return Idea.objects.filter(grappe__slug=grappe)


class IdeaDeleteView(LoginRequiredMixin, GrappeView, DeleteView):
    model = Idea
    template_name = 'grappe_ui/form.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Supprimer une envie'
        context['paragraph'] = f"Êtes vous sûr de vouloir supprimer cette envie: '{str(kwargs['object'])}'?"
        context['button_label'] = 'Confirmer'
        return context

    def delete(self, request, *args, **kwargs):
        Idea.objects.get(id=kwargs['pk']).delete()
        return HttpResponseRedirect('/grappes/{0}/ideas'.format(kwargs['grappe']))


class PoolsPerGrappeListView(LoginRequiredMixin, GrappeView, ListView):
    context_object_name = 'pools'
    template_name = 'grappe_ui/pools_per_grappe.html'
    login_url = reverse_lazy('login')

    def get_queryset(self):
        grappe = self.get_grappe_slug()
        pools = list()
        for pool_module, pool_info in PoolsRegistry.pools.items():
            pools.append({'pool_info': pool_info,
                          'pool_module': pool_module,
                          'instances_with_resources': {k: [r for r in Resource.objects.filter(pool__slug=k.slug)]
                                                       for k in pool_info.model_class.objects.filter(grappe__slug=grappe)}
                          })
        return pools


class LegalNoticeView(LoginRequiredMixin, View):
    template_name = 'grappe_ui/simple_text_page.html'

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            return render(request, self.template_name, {'title': "Mentions légales",
                                                        'content': "Bla bla bla"})
        else:
            return redirect(reverse_lazy('login'))


class CookiesNoticeView(LoginRequiredMixin, View):
    template_name = 'grappe_ui/simple_text_page.html'

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            return render(request, self.template_name, {'title': "Cookies",
                                                        'content': "Bla bla bla"})
        else:
            return redirect(reverse_lazy('login'))


class LicenseNoticeView(LoginRequiredMixin, View):
    template_name = 'grappe_ui/simple_text_page.html'

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            return render(request, self.template_name, {'title': "License",
                                                        'content': "Bla bla bla"})
        else:
            return redirect(reverse_lazy('login'))
