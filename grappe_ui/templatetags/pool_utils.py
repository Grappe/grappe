# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django import template

register = template.Library()


@register.inclusion_tag('grappe_ui/introduce_pool.html', takes_context=True)
def introduce_pool(context, *args, **kwargs):
    return {"pool": context["pool"],
            "module_name": context["module_name"],
            "grappe": context["grappe"],
            "type": context["type"],
            "display_buttons": kwargs['display_buttons'] if 'display_buttons' in kwargs.keys() else True}
