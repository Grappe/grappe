# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django import template

register = template.Library()


# To get the name of the class of an object in the templates
@register.filter(name='get_class')
def get_class(value):
    return value.__class__.__name__


@register.filter(name='get_from_member')
def get_from_member(issue):
    if len(issue.shares.all())>0:
        return issue.shares.all()[0].from_member
    else:
        return ""


@register.filter(name='get_to_members')
def get_to_members(issue):
    members = list()
    for share in issue.shares.all():
        members += share.to_members.all()
    members = list(map(lambda user: user.username, members))
    return ', '.join(members)


@register.filter(name='get_time_sum')
def get_time_sum(issue):
    return sum(share.time for share in issue.shares.all())


@register.filter(name='get_money_sum')
def get_money_sum(issue):
    return sum(share.money for share in issue.shares.all())