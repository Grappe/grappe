# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import ModelForm, HiddenInput, Form, ModelChoiceField, Select, CheckboxSelectMultiple, TextInput, \
    DateInput, FileInput, Textarea, CharField, EmailField
from grappe_core.models import Share, Grappe, Idea, UserProfile

_attrs = {'class': 'form-control'}


def add_type(input_dict, type_name):
    res = input_dict.copy()
    res['type'] = type_name
    return res


class ShareModelForm(ModelForm):

    class Meta:
        model = Share
        fields = ['from_member', 'to_members', 'date', 'description', 'time', 'money', 'tag', 'grappe']
        widgets = {'grappe': HiddenInput(),
                   'from_member': Select(attrs=_attrs),
                   'description': TextInput(attrs=_attrs),
                   'to_members': CheckboxSelectMultiple(attrs={"class": "column-checkbox members-scroll"}),
                   'time': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'money': TextInput(attrs=add_type(_attrs, 'numeric')),
                   'date': DateInput(attrs=add_type(_attrs, "date")),
                   'tag': TextInput(attrs=_attrs),
                   }

    def __init__(self, *args, **kwargs):
        grappe = Grappe.objects.get(slug=kwargs.pop('grappe'))
        super().__init__(*args, **kwargs)
        # Setting the query set of each field
        grappe_members = User.objects.filter(username__in=[m.username for m in grappe.members.all()])
        self.fields['from_member'].queryset = grappe_members
        self.fields['to_members'].queryset = grappe_members

        # Setting the label for each field
        self.fields['from_member'].label = "Donneur"
        self.fields['description'].label = "Description"
        self.fields['to_members'].label = "Receveur"
        self.fields['time'].label = f"Temps (en {grappe.time_unit})"
        self.fields['money'].label = f"Argent (en {grappe.money_unit})"
        self.fields['date'].label = "Date"
        self.fields['tag'].label = "Etiquette"

        self.fields['grappe'].initial = grappe


class AddMemberToGrappeForm(Form):
    new_member = ModelChoiceField(queryset=User.objects.all())
    new_member.widget = Select(attrs=_attrs)
    new_member.label = "Nouveau membre"


class GrappeModelForm(ModelForm):

    class Meta:
        model = Grappe
        fields = ['name', 'members', 'time_unit', 'money_unit', 'logo']
        widgets = {'name': TextInput(attrs=_attrs),
                   'members': CheckboxSelectMultiple(attrs={"class": "column-checkbox members-scroll"}),
                   'time_unit': TextInput(attrs=_attrs),
                   'money_unit': TextInput(attrs=_attrs),
                   'logo': FileInput(attrs=_attrs)
                   }

    def __init__(self, *args, **kwargs):
        self.grappe_creator = kwargs.pop('creator') if 'creator' in kwargs.keys() else None
        super().__init__(*args, **kwargs)

        # Set the labels of the fields
        self.fields['name'].label = "Nom de la grappe"
        self.fields['members'].label = "Membres"
        self.fields['time_unit'].label = "Unité de temps"
        self.fields['money_unit'].label = "Monnaie"
        self.fields['logo'].label = "Logo"

    def clean_members(self):
        members = self.cleaned_data.get('members', False)
        if self.grappe_creator and (self.grappe_creator not in members):
            raise ValidationError("Tu as oublié de t'inclure dans la grappe !")
        return members


class IdeaModelForm(ModelForm):

    class Meta:
        model = Idea
        fields = ['member', 'description', 'grappe']
        widgets = {'grappe': HiddenInput(),
                   'description': TextInput(attrs=_attrs),
                   'member': Select(attrs=_attrs)
                   }

    def __init__(self, *args, **kwargs):
        grappe = Grappe.objects.get(slug=kwargs.pop('grappe'))
        super().__init__(*args, **kwargs)
        # Set the query set for the fields
        grappe_members = User.objects.filter(username__in=[m.username for m in grappe.members.all()])
        self.fields['member'].queryset = grappe_members
        # Set the labels of the fields
        self.fields['member'].label = "Membre"
        self.fields['description'].label = "Description"


class UserProfileModelForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = ['avatar', 'presentation']
        widgets = {'avatar': FileInput(attrs=_attrs),
                   'presentation': Textarea(attrs=_attrs)
                   }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['avatar'].label = "Avatar"
        self.fields['presentation'].label = "Présentation"


class SignUpForm(UserCreationForm):
    first_name = CharField(max_length=30, required=False, help_text='Optionel.')
    last_name = CharField(max_length=30, required=False, help_text='Optionel.')
    email = EmailField(max_length=254, help_text="Requis.")

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].label = "Prénom"
        self.fields['last_name'].label = "Nom"
