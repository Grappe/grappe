# This file is part of the Grappe project.
# Copyright (C) 2020-2021 Authors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License.txt as
# published by the Free Software Foundation, version 3 of the
# License.txt any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License.txt for more details.
#
# You should have received a copy of the GNU Affero General Public License.txt
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.urls import include, path
from grappe_core.module import PoolsRegistry
from grappe_ui.views import AccountDetailView, SharesPerGrappeListView, \
    AccountsPerGrappeListView, ShareCreateView, GrappeCreateView, \
    IdeasPerGrappeListView, IdeaCreateView, IdeaDeleteView, \
    GrappeDeleteView, GrappeUpdateView, ShareDeleteView, ShareUpdateView, GrappeDetailView, HistoryView, \
    PoolsPerGrappeListView, GrappeActivityView, LegalNoticeView, CookiesNoticeView, \
    LicenseNoticeView

urlpatterns = [

    # News page for the grappe
    path('grappes/<slug:grappe>/', GrappeActivityView.as_view(), name='grappe_activity'),

    # Create a new grappe
    path('grappes/create/new', GrappeCreateView.as_view(), name='create_grappe'),

    # Profile of a grappe
    path('grappes/<slug>/config/', GrappeDetailView.as_view(), name='config_grappe'),

    # Update a grappe
    path('grappes/<slug>/update/', GrappeUpdateView.as_view(), name='update_grappe'),

    # Delete a grappe
    path('grappes/<slug>/delete/', GrappeDeleteView.as_view(), name='delete_grappe'),

    # History of the grappe
    path('grappes/<slug>/history/', HistoryView.as_view(), name='grappe_history'),

    # ~~~~~~~~~~~~~~~ Shares ~~~~~~~~~~~~~~~

    # List view of the shares of a grappe
    path('grappes/<slug:grappe>/shares/', SharesPerGrappeListView.as_view(), name='shares'),

    # Create a new share
    path('grappes/<slug:grappe>/shares/create/', ShareCreateView.as_view(), name='create_share'),

    # Update a share
    path('grappes/<slug:grappe>/shares/update/<pk>/', ShareUpdateView.as_view(), name='update_share'),

    # Delete a share
    path('grappes/<slug:grappe>/shares/delete/<pk>/', ShareDeleteView.as_view(), name='delete_share'),

    # ~~~~~~~~~~~~~~~ Accounts ~~~~~~~~~~~~~~~
    # List view of all the accounts of the grappe
    path('grappes/<slug:grappe>/accounts/', AccountsPerGrappeListView.as_view(), name='accounts'),

    # Detail view of an account of the grappe
    path('grappes/<slug:grappe>/accounts/<str:username>/', AccountDetailView.as_view(), name='member_account'),

    # ~~~~~~~~~~~~~~~ Ideas ~~~~~~~~~~~~~~~

    # List view of all the ideas of the grappe
    path('grappes/<slug:grappe>/ideas/', IdeasPerGrappeListView.as_view(), name='ideas'),

    # Create a new idea
    path('grappes/<slug:grappe>/ideas/create/', IdeaCreateView.as_view(), name='create_idea'),

    # Edit an idea
    path('grappes/<slug:grappe>/ideas/edit/<pk>/', IdeaCreateView.as_view(), name='update_idea'),

    # Delete an idea
    path('grappes/<slug:grappe>/ideas/delete/<pk>/', IdeaDeleteView.as_view(), name='delete_idea'),

    # ~~~~~~~~~~~~~~~ Pools ~~~~~~~~~~~~~~~

    # List view of all the pools of the grappe
    path('grappes/<slug:grappe>/pools/', PoolsPerGrappeListView.as_view(), name='pools'),

    # ~~~~ Miscs ~~~~~
    path('legal', LegalNoticeView.as_view(), name='legal'),
    path('cookies', CookiesNoticeView.as_view(), name='cookies'),
    path('license', LicenseNoticeView.as_view(), name='license'),

]

# For each pool module
for pool_module_name in PoolsRegistry.pools:

    # Forward the url to the module
    urlpatterns.append(path(f'grappes/<slug:grappe>/{pool_module_name}/', include(f'{pool_module_name}.urls')))
